#!/bin/bash
ip=as5local.24dial.com;
scp Cargo.toml root@${ip}:/opt/as5local/;
scp -r src/ root@${ip}:/opt/as5local/;
ssh root@${ip} 'cd /opt/as5local/ && cargo build --release && systemctl stop as5local && cp /opt/as5local/target/release/as5local /opt/as5local/ && systemctl start as5local'