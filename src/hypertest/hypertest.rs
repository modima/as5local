use std::convert::Infallible;
use std::net::SocketAddr;
use std::sync::{Arc, RwLock};
use std::sync::atomic::{AtomicI32, Ordering};
use std::time::Duration;
use hyper::{Body, Request, Response, Server};
use hyper::service::{make_service_fn, service_fn};
use log::{info};

#[derive(Clone)]
pub struct HypTest {
    _total: Arc<AtomicI32>,
    _pending: Arc<AtomicI32>,
    _max_val: Arc<RwLock<i32>>,
}

async fn _handle_request(_request: Request<Body>, hyper: HypTest) -> Result<Response<Body>, Infallible> {
    hyper._total.fetch_add(1, Ordering::Relaxed);
    hyper._pending.fetch_add(1, Ordering::Relaxed);
    let total = hyper._total.load(Ordering::Relaxed);
    let pending = hyper._pending.load(Ordering::Relaxed);
    let max = match hyper._max_val.write() {
        Ok(mut val) => {
            if *val < pending {
                *val = pending;
            }
            *val
        }
        Err(_) => { 0 }
    };

    info!("handle request - total: {}  pending: {} max: {}", total,  pending, max);

    tokio::time::sleep(Duration::from_millis(1000)).await;

    let ret = Ok(Response::new(Body::from("Hello World!")));

    hyper._pending.fetch_sub(1, Ordering::Relaxed);

    ret
}

impl HypTest {
    pub async fn _start_hyper_test() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
        let hyper = HypTest {
            _total: Arc::new(AtomicI32::new(0)),
            _pending: Arc::new(AtomicI32::new(0)),
            _max_val: Arc::new(RwLock::new(0)),
        };

        let addr = SocketAddr::from(([0, 0, 0, 0], 9090));
        let make_svc = make_service_fn(|_conn| {
            let hyper = hyper.clone();
            async move {
                Ok::<_, hyper::Error>(service_fn(move |request| {
                    _handle_request(request, hyper.clone())
                }))
            }
        });

        let server = Server::bind(&addr)
            .serve(make_svc);

        server.await?;
        Ok(())
    }
}