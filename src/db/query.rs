use crate::http::{errors::AppError, parameter::Parameter, resource::ResourceKey};
use crate::db::filter::OP;
use std::collections::HashMap;
use base64::Engine;

const DEFAULT_LIMIT: i32 = 100;

#[derive(Debug)]
pub struct StoreQuery<'a> {
    pub rkey: &'a ResourceKey,
    pub parameters: &'a HashMap<String, Parameter>,
    pub is_equality_filter: bool
}

impl<'a> StoreQuery<'a> {
    pub fn new(
        rkey: &'a ResourceKey,
        parameters: &'a HashMap<String, Parameter>,
    ) -> StoreQuery<'a> {
        let mut is_equality_filter = false;
        for p in parameters.values().cloned() {
            if !p.is_reserved() && matches!(p.operator, OP::EQ) {
                is_equality_filter = true;
                break;
            }
        }
        StoreQuery {
            rkey: rkey,
            parameters: parameters,
            is_equality_filter: is_equality_filter,
        }
    }

    pub fn get_parameter(&self, key: &str) -> Parameter {
        match self.parameters.get(&key as &str) {
            Some(p) => return p.clone(),
            None => return Parameter::new(&key, vec![]).unwrap(),
        }
    }

    pub fn is_nameonly(&self) -> bool {
        self.get_parameter("_nameonly_").as_bool()
    }

    pub fn is_showindex(&self) -> bool {
        self.get_parameter("_showindex_").as_bool()
    }

    pub fn get_limit(&self) -> i32 {
        match self.get_parameter("_limit_").as_i32() {
            Ok(limit) => {
                if limit < 1 {
                    DEFAULT_LIMIT
                } else {
                    limit
                }
            }
            Err(_) => DEFAULT_LIMIT,
        }
    }

    pub fn get_cursor(&self) -> Result<Vec<u8>, AppError> {
        let encoded = self.get_parameter("_cursor_").as_string();
        Ok(base64::engine::general_purpose::URL_SAFE_NO_PAD.decode(encoded)?)

        // Ok(base64::decode_config(encoded, base64::URL_SAFE_NO_PAD)?)
    }
}
