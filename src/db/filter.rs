use crate::db::query::StoreQuery;
use crate::http::{errors::AppError, utils};
use strum_macros::{AsRefStr, EnumIter};

const MIN_CHAR: char = '0';
const MAX_CHAR: char = '~';

#[derive(Debug, Clone, Copy, EnumIter, AsRefStr, PartialEq, PartialOrd)]
pub enum OP {
    EQ,
    LT,
    LE,
    GT,
    GE,
    SW,
    OB,
}

#[derive(Debug)]
pub struct NameFilter {
    pub desc_order: bool,
    pub rangestart: String,
    pub rangeend: String,
}

impl NameFilter {
    pub fn new(sq: &StoreQuery) -> Result<NameFilter, AppError> {
        let mut nf = NameFilter {
            desc_order: false,
            rangestart: "".to_string(),
            rangeend: MAX_CHAR.to_string(),
        };
        for p in sq.parameters.values().cloned() {
            let mut rangestart: String = "".to_string();
            let mut rangeend: String = MAX_CHAR.to_string();
            if p.attribute_name.eq("_name_") {
                let name: String = p.as_string();
                match p.operator {
                    OP::EQ => {
                        rangestart = name.clone();
                        rangeend = format!("{}{}", name, MIN_CHAR);
                    }
                    OP::LT => {
                        rangeend = name.clone();
                    }
                    OP::LE => {
                        rangeend = format!("{}{}", name, MIN_CHAR);
                    }
                    OP::GT => {
                        rangestart = format!("{}{}", name, MIN_CHAR);
                    }
                    OP::GE => {
                        rangestart = name.clone();
                    }
                    OP::SW => {
                        rangestart = name.clone();
                        rangeend = utils::get_range_limit(&name);
                    }
                    _ => {
                        return Err(AppError::new(
                            400,
                            &format!(
                                "Invalid query operator for _name_: {:?}",
                                p.operator
                            ),
                        ));
                    } /*
                      OP::OB => {
                          nf.desc_order = p.as_string().eq("desc");
                      }
                      */
                }
                if rangestart.len() > 0
                    && (nf.rangestart.len() == 0 || rangestart.cmp(&nf.rangestart).is_gt())
                {
                    nf.rangestart = rangestart;
                }

                if rangeend.len() > 0
                    && (nf.rangeend.len() == 0 || rangeend.cmp(&nf.rangeend).is_lt())
                {
                    nf.rangeend = rangeend;
                }
            }
        }
        log::debug!("StoreQuery: {:?} NameFilter: {:?}", sq, nf);
        Ok(nf)
    }

    pub fn order_as_string(&self) -> String {
        if self.desc_order {
            "desc".to_string()
        } else {
            "asc".to_string()
        }
    }
    pub fn get_rangeend(&self) -> &[u8] {
        if self.rangeend.len() > 0 {
            self.rangeend.as_bytes()
        } else {
            &[b'~']
        }
    }

    pub fn matches(&self, name: &String) -> bool {
        if self.rangestart.len() > 0 && name.cmp(&self.rangestart).is_lt() {
            return false;
        }
        if self.rangeend.len() > 0 && name.cmp(&self.rangeend).is_ge() {
            return false;
        }
        return true;
    }
}
