use crate::adapter::db::DB;
use crate::debug::debug_async_timing::DebugAsyncTiming;
use crate::http::{
    errors::AppError,
    resource::{Resource, ResourceKey},
};
use crate::index::{indexer, record::IndexSet};
use lazy_static::lazy_static;
use rand::{self, Rng};
use std::collections::HashMap;
use std::sync::{Arc, RwLock};
use tokio::sync::Mutex;

lazy_static! {
    #[derive(Debug)]
    static ref RESOURCE_LOCK:Arc<RwLock<HashMap<String, PathLockItem>>> = Arc::new(RwLock::new(HashMap::new()));

}

#[derive(Clone)]
struct PathLockItem {
    key: String,
    lease_count: Arc<RwLock<i16>>,
    lock: Arc<Mutex<usize>>,
}

impl PathLockItem {
    pub fn get_lease(rkey: &ResourceKey) -> Result<PathLockItem, String> {
        let key = rkey.get_path();

        let (is_new, ret_val): (bool, PathLockItem) = match RESOURCE_LOCK.write() {
            Ok(mut map) => {
                if let Some(lease) = map.get(&key) {
                    (false, lease.clone())
                } else {
                    //info!("leasecontrol - path: {} thread: {:?} - create lease", key, thread_id);
                    let item = PathLockItem {
                        key: key.clone(),
                        lease_count: Arc::new(RwLock::new(1)),
                        lock: Arc::new(Mutex::new(0)),
                    };
                    map.insert(key.clone(), item.clone());
                    (true, item)
                }
            }
            Err(_) => {
                return Err("resource lock is poison".to_string());
            }
        };

        // new items are incremented at create time, this reduce remove errors
        if !is_new {
            match ret_val.lease_count.write() {
                Ok(mut lease_count) => {
                    *lease_count += 1;
                }
                Err(e) => {
                    return Err(format!("{:?}", e));
                }
            }
        }
        Ok(ret_val.clone())
    }

    pub fn release(&self) {
        let lease_count = match self.lease_count.write() {
            Ok(mut val) => {
                *val -= 1;
                *val
            }
            Err(_) => 0,
        };

        // remove only if no leases still open
        if lease_count <= 0 {
            //info!("leasecontrol - path: {} thread: {:?} - remove lease", self.key, thread_id);
            match RESOURCE_LOCK.write() {
                Ok(mut map) => {
                    map.remove(&self.key);
                }
                Err(_) => {}
            }
        }
    }
}

pub async fn get(
    db: &dyn DB,
    rkey: &ResourceKey,
    debug_profiler: &DebugAsyncTiming,
) -> Result<Option<Resource>, AppError> {
    debug_profiler.measure_time("enter OP Method".to_string());

    let ret = db.get(rkey).await;

    debug_profiler.measure_time("return OP Result method".to_string());
    ret
}

pub async fn put<'a>(
    db: &dyn DB,
    rkey: &ResourceKey,
    new_rsrc: &'a mut Resource,
    debug_profiler: &DebugAsyncTiming,
) -> Result<i32, AppError> {
    debug_profiler.measure_time("enter OP Method".to_string());

    let lease = match PathLockItem::get_lease(&rkey) {
        Ok(lease) => lease,
        Err(e) => {
            log::warn!("lock resource {:?} failed with error {:?}", rkey, e);
            return Ok(-1);
        }
    };

    let mut val = lease.lock.lock().await;

    //info!("leasecontrol - path: {} thread: {:?} - enter lock put", rkey.get_path(), thread_id);

    debug_profiler.measure_time("get file lease".to_string());

    // check version
    let mut is_new_rsrc = false;
    let mut new_version = create_initial_version();
    let mut old_version = new_rsrc.version;
    let mut old_rsrc;
    let mut old_rsrc_opt = None;
    let (exit, result) = match db.get(rkey).await {
        Ok(Some(r)) => {
            // version conflict?
            if old_version >= 0 && old_version != r.version {
                log::warn!(
                    "version conflict on resource {} - old_version: {} db.version: {}",
                    rkey.get_path(),
                    old_version,
                    r.version,
                );
                (true, Ok(-1))
            } else {
                old_version = r.version;
                new_version = r.version + 1;
                old_rsrc_opt = Some(r);
                (false, Ok(0))
            }
        }
        Ok(None) => {
            is_new_rsrc = true;
            new_version = create_initial_version();
            (false, Ok(0))
        }
        Err(e) => (true, Err(e.into())),
    };

    if exit {
        lease.release();
        //info!("leasecontrol - path: {} thread: {:?} - exit lock put (0)", rkey.get_path(), thread_id);
        return result;
    }

    debug_profiler.measure_time("get resource".to_string());

    new_rsrc.version = new_version;

    log::debug!(
        "PUT {} {} -> {}",
        rkey.get_path(),
        old_version,
        new_rsrc.version,
    );

    // update indeces
    let mut old_idx_set;
    let mut new_viewentry_ids = Vec::new();
    let (exit, result) = if rkey.get_content_type().eq("application/json") {
        // create indeces (new resource and old (if any))
        let mut old_rows_opt = None;
        if !old_rsrc_opt.is_none() {
            old_rsrc = old_rsrc_opt.unwrap();
            old_rows_opt = match IndexSet::new(rkey, &mut old_rsrc, true) {
                Ok(is) => {
                    old_idx_set = is;
                    Some(&mut old_idx_set.rows)
                }
                Err(e) => {
                    log::warn!("Old index error: {}", e);
                    None
                }
            }
        };

        let mut is = IndexSet::new(rkey, new_rsrc, false)?;
        let mut result = indexer::IndexRefreshResultHolder {
            putlist: Vec::new(),
            dellist: Vec::new(),
        };

        new_viewentry_ids = indexer::refresh_indeces(&mut is.rows, old_rows_opt, &mut result);
        log::debug!("new_viewentry_ids: {:?}", new_viewentry_ids);

        let (exit, ret_result): (bool, Result<i32, AppError>) = if result.dellist.len() > 0 {
            match db.delete_index_rows(result.dellist).await {
                Ok(_) => (false, Ok(0)),
                Err(e) => (true, Err(e.into())),
            }
        } else {
            (false, Ok(0))
        };

        if exit {
            (true, ret_result)
        } else {
            if result.putlist.len() > 0 {
                match db.insert_index_rows(result.putlist).await {
                    Ok(_) => (false, Ok(0)),
                    Err(e) => (true, Err(e.into())),
                }
            } else {
                (false, Ok(0))
            }
        }
    } else {
        (false, Ok(0))
    };

    if exit {
        // remove write lock from resource
        lease.release();
        //info!("leasecontrol - path: {} thread: {:?} - exit lock put (1)", rkey.get_path(), thread_id);
        return result;
    }

    debug_profiler.measure_time("build idx".to_string());

    //save resource
    new_rsrc.viewentry_ids = new_viewentry_ids;
    let result = if is_new_rsrc {
        db.insert(new_rsrc).await
    } else {
        db.update(new_rsrc, old_version).await
    };

    let ret = match result {
        Ok(success) => {
            if success {
                Ok(new_version)
            } else {
                log::warn!("update {} failed (version changed)", rkey.get_path());
                Ok(-1)
            }
        }
        Err(e) => {
            if is_new_rsrc {
                log::warn!("insert {} failed with {}", rkey.get_path(), e,);
            } else {
                log::warn!("update {} failed with {}", rkey.get_path(), e,);
            };
            Err(e.into())
        }
    };
    debug_profiler.measure_time("return OP Result method".to_string());

    *val += 1;
    lease.release();
    //info!("leasecontrol - path: {} thread: {:?} - exit lock put (ende lockcount: {})", rkey.get_path(), thread_id, *val);
    ret
}

pub async fn delete(
    db: &dyn DB,
    rkey: &ResourceKey,
    old_version: i32,
    debug_profiler: &DebugAsyncTiming,
) -> Result<bool, AppError> {
    debug_profiler.measure_time("enter OP Method".to_string());

    // create write lock on resource
    let lease = match PathLockItem::get_lease(&rkey) {
        Ok(lease) => lease,
        Err(e) => {
            log::warn!("lock resource {:?} failed with error {:?}", rkey, e);
            return Ok(false);
        }
    };

    let mut val = lease.lock.lock().await;

    //info!("leasecontrol - path: {} thread: {:?} - enter lock delete", rkey.get_path(), thread_id);

    debug_profiler.measure_time("get file lease".to_string());

    let (exit, ret_result) = if rkey.get_content_type().eq("application/json") {
        match db.get(rkey).await {
            Ok(Some(mut old_rsrc)) => {
                debug_profiler.measure_time("read db version done".to_string());

                // version conflict?
                if old_version >= 0 && old_version != old_rsrc.version {
                    log::warn!(
                        "version conflict on resource {} - old_version: {} db.version: {}",
                        rkey.get_path(),
                        old_version,
                        old_rsrc.version,
                    );
                    lease.release();
                    (true, Ok(false))
                } else {
                    let mut idx_set = IndexSet::new(rkey, &mut old_rsrc, true)?;
                    let mut result = indexer::IndexRefreshResultHolder {
                        putlist: Vec::new(),
                        dellist: Vec::new(),
                    };
                    indexer::refresh_indeces(&mut idx_set.rows, None, &mut result);
                    debug_profiler.measure_time("refresh indexes".to_string());

                    match db.delete_index_rows(result.putlist).await {
                        Ok(_) => {
                            debug_profiler.measure_time("delete indexes".to_string());
                            (false, Ok(false))
                        }
                        Err(e) => {
                            lease.release();
                            (true, Err(e.into()))
                        }
                    }
                }
            }
            Ok(None) => (false, Ok(false)),
            Err(e) => (true, Err(e.into())),
        }
    } else {
        (false, Ok(false))
    };

    if exit {
        lease.release();
        //info!("leasecontrol - path: {} thread: {:?} - exit lock delete(0)", rkey.get_path(), thread_id);
        return ret_result;
    }

    *val += 1;
    let result = db.delete(rkey).await;
    lease.release();
    //info!("leasecontrol - path: {} thread: {:?} - exit lock delete (ende lockcount: {})", rkey.get_path(), thread_id, *val);
    result
}

fn create_initial_version() -> i32 {
    let mut rng = rand::thread_rng();
    rng.gen_range(0..i32::MAX)
}
