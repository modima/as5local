use crate::adapter::db::DB;
use crate::db::{filter::NameFilter, query::StoreQuery};
use crate::http::{
    errors::AppError,
    results::{Record, ResultSet},
};
use crate::index::{filter::IndexFilter, results::IndexResultSet};

async fn add_file_listing(
    db: &dyn DB,
    rs: &mut ResultSet,
    path: &String,
    namefilter: &NameFilter,
    limit: i32,
) {
    match db.list_files(path, &namefilter, limit - rs._count_).await {
        Ok(rows) => {
            let mut iter = rows.into_iter();
            loop {
                match iter.next() {
                    Some(row) => {
                        if namefilter.matches(&row.name) {
                            rs.add_result(Record { _name_: row.name });
                            if rs._count_ == limit {
                                log::debug!("{} files", rs._count_);
                                return;
                            }
                        }
                    }
                    None => return,
                }
            }
        }
        Err(e) => {
            log::error!("{}", e);
            return;
        }
    }
}

async fn add_dir_listing(
    db: &dyn DB,
    rs: &mut ResultSet,
    path: &String,
    namefilter: &NameFilter,
    limit: i32,
) {
    match db.list_dirs(path, &namefilter, limit - rs._count_).await {
        Ok(rows) => {
            let mut iter = rows.into_iter();
            loop {
                match iter.next() {
                    Some(row) => {
                        let dirname = row.name + "/";
                        if dirname.len() > 1 && namefilter.matches(&dirname) {
                            rs.add_result(Record { _name_: dirname });
                            if rs._count_ == limit {
                                log::debug!("{} directories", rs._count_);
                                return;
                            }
                        }
                    }
                    None => return,
                }
            }
        }
        Err(e) => {
            log::error!("{}", e);
            return;
        }
    }
}

async fn add_view_listing(
    db: &dyn DB,
    rs: &mut ResultSet,
    path: &String,
    namefilter: &NameFilter,
    limit: i32,
) {
    match db.list_views(path, &namefilter, limit - rs._count_).await {
        Ok(rows) => {
            let mut iter = rows.into_iter();
            loop {
                match iter.next() {
                    Some(row) => {
                        if namefilter.matches(&row.name) {
                            rs.add_result(Record {
                                _name_: row.name + "//",
                            });
                            if rs._count_ == limit {
                                log::debug!("{} views", rs._count_);
                                return;
                            }
                        }
                    }
                    None => return,
                }
            }
        }
        Err(e) => {
            log::error!("{}", e);
            return;
        }
    }
}

pub async fn get_directory_listing(
    db: &dyn DB,
    sq: &StoreQuery<'_>,
) -> Result<ResultSet, AppError> {
    let mut rtype = sq.get_parameter("_type_").as_string().to_lowercase();
    if rtype.len() == 0 {
        rtype = "vdf".to_string();
    }
    let namefilter: NameFilter = NameFilter::new(sq)?;
    let limit: i32 = sq.get_limit();
    let mut rs: ResultSet = ResultSet::new();
    let path = sq.rkey.get_path();

    log::debug!("list dir path: {} type: {}", path, rtype);

    for (i, c) in rtype.chars().enumerate() {
        if rs._count_ >= limit {
            break;
        }
        // only first appearance
        if Some(i) == rtype.find(c) {
            match c {
                'v' => {
                    add_view_listing(db, &mut rs, &path, &namefilter, limit).await;
                }
                'd' => {
                    add_dir_listing(db, &mut rs, &path, &namefilter, limit).await;
                }
                'f' => {
                    add_file_listing(db, &mut rs, &path, &namefilter, limit).await;
                }
                _ => {
                    log::warn!("Invalid resource type {}", c);
                }
            }
        }
    }
    Ok(rs)
}

pub async fn query_index(db: &dyn DB, sq: &StoreQuery<'_>) -> Result<IndexResultSet, AppError> {
    let namefilter = NameFilter::new(sq)?;
    let indexfilter = IndexFilter::new(sq)?;
    let limit: i32 = sq.get_limit();
    let path = sq.rkey.get_path();
    log::debug!("query index path: {}", path);
    db.filter_index(
        &sq.rkey.dir,
        &sq.rkey.name,
        &namefilter,
        &indexfilter,
        limit,
    )
    .await
}
