mod adapter;
mod db;
pub mod debug;
pub mod error;
mod http;
mod hypertest;
mod index;
mod profiling;
pub mod webserver;

use crate::debug::debug_async_timing::DebugAsyncTiming;
use crate::debug::quantile_stat::QuantilStat;
use crate::http::errors::AppError;
use crate::profiling::{
    profiler,
    profiler::{Message, MessageType},
};
use crate::webserver::tls_server::TlsServer;
use adapter::{db::DB, pg, rocksdb};
use bytes::Bytes;
use crossbeam::channel::Sender;
use db::{op, query::StoreQuery, store};
use http::{
    parameter,
    resource::{Resource, ResourceKey},
    response::Response,
    utils,
};
use hyper::{
    server::conn::AddrStream,
    service::{make_service_fn, service_fn},
    {Method, Request, Server},
};
use lazy_static::lazy_static;
use log::{error, info};
use multer::Multipart;
use std::error::Error;
use std::fs::File;
use std::io::Read;
use std::path::Path;
use std::sync::atomic::Ordering::Relaxed;
use std::time::Duration;
use std::{
    env,
    net::SocketAddr,
    sync::{Arc, Mutex, RwLock},
    thread,
    time::SystemTime,
};
use strum_macros::AsRefStr;
use tokio::time::Instant;
use urlencoding::decode as url_decode;
use uuid::Uuid;

lazy_static! {
    #[derive(Debug)]
    static ref DB_TYPE : String = dotenv::var("DATABASE").unwrap_or("rocksdb".to_string());

    static ref DB_INSTANCE: Box<dyn DB> = if DB_TYPE.as_str() == "postgresql" {
        pg::PostgresDB::create()
    } else {
        rocksdb::RocksDB::create()
    };

    pub static ref REUQEST_HANDLER: Arc<RequestHandler> =Arc::new(RequestHandler {
            db: if DB_TYPE.as_str() == "postgresql" {
                pg::PostgresDB::create()
            } else {
                rocksdb::RocksDB::create()
            },
            quantile_stat: QuantilStat::new(),
            auth_token: Arc::new(RwLock::new(String::new()))
    });

    pub static  ref PROFILER_INSTANCE : Arc<Mutex<profiler::Profiler>> = Arc::new(Mutex::new( profiler::Profiler::new(REUQEST_HANDLER.clone())));
}

#[derive(Debug, Eq, PartialEq, PartialOrd, Hash, AsRefStr, Clone, Copy)]
pub enum RequestType {
    GET,
    PUT,
    POST,
    DELETE,
    OPTIONS,
    OTHER,
}

pub struct RequestHandler {
    quantile_stat: QuantilStat,
    db: Box<dyn DB>,
    auth_token: Arc<RwLock<String>>,
}

impl RequestHandler {
    async fn body_to_bytes(req: Request<hyper::Body>) -> Result<Vec<u8>, hyper::Error> {
        let body_bytes = hyper::body::to_bytes(req.into_body()).await?;
        let v = body_bytes.to_vec();
        Ok(v)
    }

    async fn handle(
        &self,
        req: Request<hyper::Body>,
        auth_token: String,
    ) -> Result<hyper::Response<hyper::Body>, hyper::http::Error> {
        let method = get_request_type(&req);
        let method_string = format!("{:?}", method);

        let request_start = Instant::now();

        log::debug!("request uri: {}", req.uri());

        let mut if_match_version = -1;
        let mut if_none_match_version = -1;
        let mut path = req.uri().path().to_string();
        let headers = req.headers();
        let mut response = Response::new();

        // CORS preflight
        let mut is_cors = false;
        if let Some(h) = headers.get("Origin") {
            is_cors = h.to_str().unwrap().len() > 0;
        }
        if is_cors {
            response.add_header("Access-Control-Allow-Origin", "*");
            if method == RequestType::OPTIONS {
                response.add_header("Access-Control-Max-Age", "86400");
                response.add_header("Allow", "GET, POST, PUT, DELETE, OPTIONS");
                if let Some(h) = req.headers().get("Access-Control-Request-Method") {
                    response.add_header("Access-Control-Allow-Methods", h.to_str().unwrap());
                }
                if let Some(h) = req.headers().get("Access-Control-Request-Headers") {
                    response.add_header("Access-Control-Allow-Headers", h.to_str().unwrap());
                }
                return response.set_status(200).build();
            }
        }

        // content type
        let mut ct_header = "";

        //info!("auth token to accpept requests: {} received: {:?}", auth_token, headers.get("Authorization"));

        // ensure that the caller knows the secret
        if !auth_token.is_empty() {
            if let Some(secret) = headers.get("Authorization") {
                if !auth_token.eq(secret.to_str().unwrap_or("")) {
                    return response
                        .set_error(&AppError {
                            status: 403,
                            message: "Forbidden".to_string(),
                        })
                        .build();
                }
            } else {
                return response
                    .set_error(&AppError {
                        status: 403,
                        message: "Forbidden".to_string(),
                    })
                    .build();
            }
        }

        if let Some(h) = headers.get("Content-Type") {
            ct_header = h.to_str().unwrap();
        }

        // version management
        if let Some(h) = headers.get("If-Match") {
            if_match_version = utils::parse_version_tag(h.to_str().unwrap());
        }
        if let Some(h) = headers.get("If-None-Match") {
            if_none_match_version = utils::parse_version_tag(h.to_str().unwrap());
        }

        log::debug!(
            "if_match_version: {} | if_none_match_version: {}",
            if_match_version,
            if_none_match_version
        );

        path = path.replace("\\+", "%2B");
        let mut key = url_decode(&path).unwrap().into_owned();

        if key.len() == 0 {
            key = "/".to_string();
        }

        let rkey: ResourceKey;
        let query_params;
        match parameter::parse(&req) {
            Ok(q) => {
                query_params = q;
            }
            Err(e) => {
                return response.set_error(&e).build();
            }
        }
        match ResourceKey::new(&key) {
            Ok(k) => rkey = k,
            Err(e) => {
                return response.set_error(&e).build();
            }
        }
        let ct = rkey.get_content_type();
        let cs = match query_params.get("_charset_") {
            Some(p) => p.as_string(),
            None => utils::guess_charset_from_content_type(&ct),
        };
        let content_type = format!("{};charset={}", ct, cs);
        response.add_header("Content-Type", &content_type);
        response.add_header("Cache-Control", "no-transform, no-cache");
        let debug_profiler = DebugAsyncTiming::new(method_string.clone(), &rkey);
        /* PUT REQUEST */
        match method {
            RequestType::PUT => {
                return if rkey.is_file() {
                    let mut encoding = None;
                    let mut rsrc_bytes: Bytes = Bytes::new();
                    let mut views_bytes: Option<Bytes> = None;
                    // handle multipart request
                    if ct_header.len() > 0 && ct_header.starts_with("multipart") {
                        match multer::parse_boundary(ct_header) {
                            Ok(boundary) => {
                                let mut multipart = Multipart::new(req.into_body(), boundary);
                                loop {
                                    match multipart.next_field().await {
                                        Ok(next) => match next {
                                            Some(field) => {
                                                let mut name = "";
                                                if let Some(n) = field.name() {
                                                    if n.len() > 0 {
                                                        name = n;
                                                    }
                                                }
                                                if let Some(n) = field.file_name() {
                                                    if n.len() > 0 {
                                                        name = n;
                                                    }
                                                }
                                                log::debug!("found field: {}", name);
                                                if name.eq("resource") {
                                                    log::debug!("is ressource");
                                                    if let Some(enc) =
                                                        field.headers().get("x-encoding")
                                                    {
                                                        log::debug!("resource encoding: {:?}", enc);
                                                        encoding =
                                                            Some(enc.to_str().unwrap().to_string());
                                                    }
                                                    rsrc_bytes = field.bytes().await.unwrap();
                                                } else if name.eq("views") {
                                                    log::debug!("is views");
                                                    views_bytes =
                                                        Some(field.bytes().await.unwrap());
                                                }
                                            }
                                            None => break,
                                        },
                                        Err(e) => {
                                            error!("error while iterate multipart: {:?}", e);
                                            if format!("{:?}", e)
                                                .contains("incomplete multipart stream")
                                            {
                                                return response
                                                    .set_error(&AppError {
                                                        status: 503,
                                                        message: "incomplete multipart stream"
                                                            .to_string(),
                                                    })
                                                    .build();
                                            }

                                            return response.set_error(&e.into()).build();
                                        }
                                    }
                                }
                            }
                            Err(e) => {
                                error!("error while iterate multipart: {:?}", e);
                                return response.set_error(&e.into()).build();
                            }
                        }
                        // handle singlepart request
                    } else {
                        rsrc_bytes = match RequestHandler::body_to_bytes(req).await {
                            Ok(b) => Bytes::from(b),
                            Err(e) => return response.set_error(&e.into()).build(),
                        }
                    }

                    let old_version = if if_none_match_version == 0 {
                        0
                    } else {
                        if_match_version
                    };
                    let modified = humantime::format_rfc3339_millis(SystemTime::now()).to_string();
                    let mut rsrc = match Resource::new(
                        rkey.dir.clone(),
                        rkey.name.clone(),
                        rsrc_bytes,
                        views_bytes,
                        old_version,
                        modified,
                        Vec::new(),
                        encoding,
                        false,
                    ) {
                        Ok(r) => r,
                        Err(e) => return response.set_error(&e).build(),
                    };

                    debug_profiler.measure_time("from start to execute DB Operation".to_string());

                    let ret = match op::put(&*self.db, &rkey, &mut rsrc, &debug_profiler).await {
                        Ok(-1) => response.set_status(409).build(),
                        Ok(version) => {
                            let etag = format!("W/{}", version);
                            response
                                .set_status(204)
                                .add_header("Etag", &etag)
                                .remove_header("Content-Type")
                                .add_header("Content-Length", "0")
                                .build()
                        }
                        Err(e) => response.set_error(&e).build(),
                    };

                    debug_profiler.measure_time("received async reuslt in main".to_string());

                    self.quantile_stat.add_value(
                        format!("{}-99 - WALLCLOCK TIME", method_string.clone()),
                        request_start.elapsed(),
                    );
                    self.quantile_stat.update_data(&debug_profiler);

                    ret
                } else {
                    response.set_status(405).build()
                };
            }
            RequestType::GET => {
                return if rkey.is_file() {
                    let old_version = if_none_match_version;
                    let mut filename;
                    debug_profiler.measure_time("from start to execute DB Operation".to_string());
                    let ret = match op::get(&*self.db, &rkey, &debug_profiler).await {
                        Ok(Some(r)) => {
                            let download = match query_params.get("_download_") {
                                Some(p) => p.as_bool(),
                                None => false,
                            };
                            filename = match query_params.get("_filename_") {
                                Some(n) => n.as_string(),
                                None => "".to_string(),
                            };
                            let cd_header;
                            if download || filename.len() > 0 {
                                if filename.len() == 0 {
                                    filename = rkey.name;
                                }
                                cd_header = format!("attachment; filename={}", filename);
                                response.add_header("Content-Disposition", &cd_header);
                            }
                            if r.version == old_version {
                                response.set_status(304).build()
                            } else {
                                response.set_status(200).set_resource(&r).build()
                            }
                        }
                        Ok(None) => response.set_status(404).build(),
                        Err(e) => response.set_error(&e).build(),
                    };

                    self.quantile_stat.add_value(
                        format!("{}-99 - WALLCLOCK TIME", method_string.clone()),
                        request_start.elapsed(),
                    );
                    debug_profiler.measure_time("received async reuslt in main".to_string());
                    self.quantile_stat.update_data(&debug_profiler);
                    ret
                } else if rkey.is_directory() {
                    log::debug!("list directory {}", rkey.get_path());
                    let q: StoreQuery = StoreQuery::new(&rkey, &query_params);
                    debug_profiler.measure_time("from start to execute DB Operation".to_string());
                    let ret = match store::get_directory_listing(&*self.db, &q).await {
                        Ok(rs) => response.set_results(&rs).build(),
                        Err(e) => response.set_error(&e).build(),
                    };
                    self.quantile_stat.add_value(
                        format!("{}-99 - WALLCLOCK TIME", method_string.clone()),
                        request_start.elapsed(),
                    );
                    debug_profiler.measure_time("received async reuslt in main".to_string());
                    self.quantile_stat.update_data(&debug_profiler);
                    ret
                } else if rkey.is_index() {
                    let q: StoreQuery = StoreQuery::new(&rkey, &query_params);
                    debug_profiler.measure_time("from start to execute DB Operation".to_string());
                    let ret = match store::query_index(&*self.db, &q).await {
                        Ok(irs) => {
                            log::debug!("is_equality_filter: {}", q.is_equality_filter);
                            //log::debug!("results: {:?}", irs);
                            response
                                .set_index_results(
                                    &irs,
                                    q.is_nameonly(),
                                    q.is_showindex(),
                                    !q.is_equality_filter,
                                )
                                .build()
                        }
                        Err(e) => response.set_error(&e).build(),
                    };
                    debug_profiler.measure_time("received async reuslt in main".to_string());
                    self.quantile_stat.add_value(
                        format!("{}-99 - WALLCLOCK TIME", method_string.clone()),
                        request_start.elapsed(),
                    );
                    self.quantile_stat.update_data(&debug_profiler);
                    ret
                } else {
                    response.set_status(405).build()
                };
            }
            RequestType::DELETE => {
                debug_profiler.measure_time("from start to execute DB Operation".to_string());
                let ret =
                    match op::delete(&*self.db, &rkey, if_match_version, &debug_profiler).await {
                        Ok(false) => response.set_status(409).build(),
                        Ok(true) => response
                            .set_status(204)
                            .remove_header("Content-Type")
                            .add_header("Content-Length", "0")
                            .build(),
                        Err(e) => response.set_error(&e).build(),
                    };
                debug_profiler.measure_time("received async reuslt in main".to_string());
                self.quantile_stat.add_value(
                    format!("{}-99 - WALLCLOCK TIME", method_string.clone()),
                    request_start.elapsed(),
                );
                self.quantile_stat.update_data(&debug_profiler);
                return ret;
            }
            _ => {
                log::warn!("method type '{}' not yet implemented", method.as_ref());
            }
        }
        response.set_status(405).build()
    }
}

pub fn get_request_type(req: &Request<hyper::Body>) -> RequestType {
    match req.method() {
        &Method::GET => RequestType::GET,
        &Method::PUT => RequestType::PUT,
        &Method::DELETE => RequestType::DELETE,
        &Method::POST => RequestType::POST,
        &Method::OPTIONS => RequestType::OPTIONS,
        _ => RequestType::OTHER,
    }
}

pub fn start_request(
    chan_profiler: Option<Sender<Message>>,
    rtype: RequestType,
    rid: &str,
    remote_ip: &str,
) {
    if let Some(tx) = chan_profiler {
        match tx.send(Message::new(
            MessageType::Start,
            Some(rid.to_string()),
            Some(rtype),
            Some(remote_ip.to_string()),
        )) {
            Ok(_) => {}
            Err(e) => {
                log::warn!("channel error on start request: {}", e);
            }
        };
    }
}

pub fn stop_request(
    chan_profiler: Option<Sender<Message>>,
    rtype: RequestType,
    rid: &str,
    remote_ip: &str,
) {
    if let Some(tx) = chan_profiler {
        match tx.send(Message::new(
            MessageType::Stop,
            Some(rid.to_string()),
            Some(rtype),
            Some(remote_ip.to_string()),
        )) {
            Ok(_) => {}
            Err(e) => {
                log::warn!("channel error on stop request: {}", e);
            }
        }
    };
}

#[tokio::main(worker_threads = 2048)]
pub async fn main2() -> Result<(), Box<dyn Error + Send + Sync>> {
    let log_config = "log4rs.yml";

    if !Path::new(log_config).exists() {
        panic!("log config: {} not found", log_config);
    }

    // initialisieren des logers
    log4rs::init_file(log_config, Default::default()).unwrap();

    // loggen von panics
    log_panics::init();

    //info!("{}",Utc::now().to_rfc3339());

    Ok(())
}

#[tokio::main(worker_threads = 2048)]
pub async fn main() {
    let log_config = "log4rs.yml";

    if !Path::new(log_config).exists() {
        panic!("log config: {} not found", log_config);
    }

    // initialisieren des logers
    log4rs::init_file(log_config, Default::default()).unwrap();

    // loggen von panics
    log_panics::init();

    // laden der ssl Config
    let args: Vec<String> = env::args().collect();

    let mut ssl_config = String::new();

    for arg in args {
        if arg.find('=').is_some() {
            let vals: Vec<&str> = arg.split('=').collect();
            let key = vals[0].to_string();
            let value = vals[1].to_string();
            if key == "sslConfig" {
                ssl_config = value;
            }
        }
    }

    let http_port = match &dotenv::var("HTTP_PORT") {
        Ok(port) => port.parse().unwrap_or(0),
        Err(_) => 0,
    };

    let https_port = match &dotenv::var("HTTPS_PORT") {
        Ok(port) => port.parse().unwrap_or(0),
        Err(_) => 0,
    };
    let http_bind_address = match &dotenv::var("HTTP_BIND_ADDRESS") {
        Ok(address) => address.clone(),
        Err(_) => "127.0.0.1".to_string(),
    };
    let https_bind_address = match &dotenv::var("HTTPS_BIND_ADDRESS") {
        Ok(address) => address.clone(),
        Err(_) => "127.0.0.1".to_string(),
    };

    let profiling_interval = match &dotenv::var("PROFILING_INTERVAL_SECONDS") {
        Ok(ivs) => ivs.parse().unwrap(),
        Err(_) => 0,
    };

    if https_port > 0 && ssl_config.is_empty() {
        log::error!("please add parameter: sslConfig=pathToConfig");
        std::process::abort();
    }

    if https_port > 0 && !Path::new(ssl_config.as_str()).exists() {
        log::error!("ssl context file not found at: {}", ssl_config);
        std::process::abort();
    }

    info!(
        "before start tls server - is config available: {}",
        !ssl_config.is_empty()
    );
    if !ssl_config.is_empty() {
        let ssl_config = match File::open(ssl_config.clone()) {
            Ok(mut file) => {
                let mut json_str = String::new();
                match file.read_to_string(&mut json_str) {
                    Ok(_) => {}
                    Err(e) => {
                        log::error!(
                            "error whi le loading ssl config: {} error: {:?}",
                            ssl_config,
                            e
                        );
                        std::process::abort();
                    }
                }
                match json::parse(json_str.as_str()) {
                    Ok(json) => json,
                    Err(e) => {
                        log::error!("error while parsing ssl config: {:?}", e);
                        std::process::abort();
                    }
                }
            }
            Err(e) => {
                log::error!("error while reading ssl config: {:?}", e);
                std::process::abort();
            }
        };

        let auth_token = ssl_config["auth_token"].as_str().unwrap_or("").to_string();

        if !auth_token.is_empty() {
            match REUQEST_HANDLER.auth_token.write() {
                Ok(mut val) => {
                    *val = auth_token;
                }
                Err(_) => {
                    error!("error while unlocking auth_token field in request_handler");
                }
            }
        }

        match TlsServer::start(ssl_config.clone(), https_bind_address, https_port).await {
            Ok(_) => {}
            Err(e) => {
                error!("error while starting https server: {:?}", e);

                // return Err(Box::new(e));
                return;
            }
        }
    }

    let pending_request_counter = TlsServer::request_counter();
    let max_concurrent_requests = TlsServer::max_concurrent_requests();    

    if let Err(e) = REUQEST_HANDLER.db.init().await {
        log::error!("database error: {}", e);
        std::process::abort();
    }
    REUQEST_HANDLER.db.schedule_backup().await;
    log::info!("Successfully connected to {:?} database", DB_TYPE.as_str());

    let mut tx2 = None;
    if profiling_interval > 0 {
        let tx = PROFILER_INSTANCE.lock().unwrap().create_channel();
        let ret = tx.clone();
        tokio::spawn(async move {
            profiler::start_receiver(PROFILER_INSTANCE.clone()).await
        });
        tokio::spawn(async move { profiler::start_profiling(tx, profiling_interval).await });
        tx2 = Some(ret)
    }

    if http_port > 0 {
        let rh = REUQEST_HANDLER.clone();

        let auth_token = match REUQEST_HANDLER.auth_token.read() {
            Ok(s) => s.clone(),
            Err(_) => String::new(),
        };

        let make_svc = make_service_fn(move |conn: &AddrStream| {
            let auth_token = auth_token.clone();
            let remote_ip = conn.remote_addr().ip().to_string();
            let rh = rh.clone();
            let tx2 = tx2.clone();
            let pending_request_counter = pending_request_counter.clone();
            let max_concurrent_requests = max_concurrent_requests.clone();
            async move {
                let auth_token = auth_token.clone();
                Ok::<_, hyper::http::Error>(service_fn(move |req| {
                    let auth_token = auth_token.clone();
                    let rh = rh.clone();
                    let tx2 = tx2.clone();
                    let remote_ip = remote_ip.clone();

                    let pending_request_counter = pending_request_counter.clone();
                    let max_concurrent_requests = max_concurrent_requests.clone();

                    async move {
                        let started = Instant::now();

                        pending_request_counter.fetch_add(1, Relaxed);

                        let pending = pending_request_counter.load(Relaxed);
                        let mut max = max_concurrent_requests.load(Relaxed);
                        if max < pending {
                            max_concurrent_requests.store(pending, Relaxed);
                            max = pending;
                        }

                        let request_type = get_request_type(&req);
                        let request_id = Uuid::new_v4().as_simple().to_string();
                        start_request(tx2.clone(), request_type, &request_id, &remote_ip);

                        let resp = rh.handle(req, auth_token.clone()).await;

                        stop_request(tx2, request_type, &request_id, &remote_ip);

                        pending_request_counter.fetch_sub(1, Relaxed);
                        info!(
                            "handle incoming request - pending: {} max: {} duration: {:?}",
                            pending,
                            max,
                            started.elapsed()
                        );
                        resp
                    }
                }))
            }
        });

        let addr = SocketAddr::from(
            format!("{}:{}", http_bind_address, http_port)
                .parse::<SocketAddr>()
                .unwrap(),
        );

        let server = Server::bind(&addr).serve(make_svc);
        info!("HTTP server started, bound on {}", addr);

        if let Err(e) = server.await {
            log::error!("server error: {}", e);
            std::process::abort();
        }
    } else {
        loop {
            thread::sleep(Duration::from_secs(120))
        }
    }
}
