use std::io;
use std::fs::File;
use std::io::{Error};
use std::panic::Location;
use std::sync::{Arc};
use hyper::server::accept::Accept;
use hyper::server::conn::{AddrIncoming, AddrStream};
use hyper::service::{make_service_fn, service_fn};
use crate::error::modima_error::ModimaError;
use std::future::Future;
use std::pin::Pin;
use std::vec::Vec;
use tokio::io::{AsyncRead, AsyncWrite, ReadBuf};
use tokio_rustls::rustls::ServerConfig;
use core::task::{Context, Poll};
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering::Relaxed;
use futures_util::ready;
use hyper::Server;
use json::JsonValue;
use crate::{get_request_type, PROFILER_INSTANCE, REUQEST_HANDLER, start_request, stop_request};
use lazy_static::lazy_static;
use log::{error, info};
use uuid::Uuid;

#[derive(Clone)]
pub struct TlsServer {}
// bsp implentierung
// https://github.com/rustls/hyper-rustls/blob/main/examples/server.rs

lazy_static! {
    #[derive(Debug)]
    static ref REQUEST_COUNTER: Arc<AtomicUsize> =Arc::new(AtomicUsize::new(0));
    static ref MAX_CONCURRENT_REQUESTS: Arc<AtomicUsize> = Arc::new(AtomicUsize::new(0));
}
impl TlsServer {
    pub fn request_counter() -> Arc<AtomicUsize> { REQUEST_COUNTER.clone() }
    pub fn max_concurrent_requests() -> Arc<AtomicUsize> { MAX_CONCURRENT_REQUESTS.clone() }
    pub async fn start(ssl_config: JsonValue, https_bind_address: String, https_port: u16) -> Result<(), ModimaError> {
        let cert_file = ssl_config["ssl_certificate"].as_str().unwrap_or("");
        let key_file = ssl_config["ssl_certificate_key"].as_str().unwrap_or("");

        let certs = match TlsServer::load_certs(cert_file) {
            Ok(certs) => {
                certs
            }
            Err(e) => {
                return Err(ModimaError::new_from_error(Location::caller(), Box::new(e)));
            }
        };

        let key = match TlsServer::load_private_key(key_file) {
            Ok(certs) => {
                certs
            }
            Err(e) => {
                return Err(ModimaError::new_from_error(Location::caller(), Box::new(e)));
            }
        };

        info!("bind address for https: {}:{}", https_bind_address, https_port);

        let addr = format!("{}:{}", https_bind_address, https_port).parse().unwrap();

        let tls_cfg = {
            // Load public certificate.
            let certs = certs;
            // Load private key.
            let key = key;
            // Do not use client certificate authentication.

            let mut cfg = match ServerConfig::builder()
                .with_safe_defaults()
                .with_no_client_auth()
                .with_single_cert(certs, key) {
                Ok(cfg) => { cfg }
                Err(e) => {
                    return Err(ModimaError::new_from_error(Location::caller(), Box::new(e)));
                }
            };
            // Configure ALPN to accept HTTP/2, HTTP/1.1 in that order.
            cfg.alpn_protocols = vec![b"h2".to_vec(), b"http/1.1".to_vec()];
            Arc::new(cfg)
        };


        // Create a TCP lis
        // Create a TCP listener via tokio.
        let incoming = match AddrIncoming::bind(&addr) {
            Ok(x) => { x }
            Err(e) => {
                return Err(ModimaError::new_from_error(Location::caller(), Box::new(e)));
            }
        };

        let auth_token = match REUQEST_HANDLER.auth_token.read() {
            Ok(s) => {
                let x = format!("{}", s);
                x
            }
            Err(_) => { "".to_string() }
        };

        let service = make_service_fn(move |_| {
            let auth_token = auth_token.clone();
            async move {
                let auth_token = auth_token.clone();
                Ok::<_, Error>(service_fn(move |req| {
                    let tx = {
                        let ret = PROFILER_INSTANCE.lock().unwrap().get_sender().clone();
                        ret
                    };
                    let auth_token = auth_token.clone();
                    async move {
                        let tx = tx.clone();
                        let remote_ip = String::new();
                        let request_handler = REUQEST_HANDLER.clone();
                        REQUEST_COUNTER.fetch_add(1, Relaxed);

                        let pending = REQUEST_COUNTER.load(Relaxed);
                        let max = MAX_CONCURRENT_REQUESTS.load(Relaxed);
                        if max < pending {
                            MAX_CONCURRENT_REQUESTS.store(pending, Relaxed);
                        }
                        let request_type = get_request_type(&req);
                        let request_id = Uuid::new_v4().as_simple().to_string();

                        start_request(tx.clone(), request_type, &request_id, &remote_ip);

                        let resp = request_handler.handle(req, auth_token.clone()).await;

                        stop_request(tx.clone(), request_type, &request_id, &remote_ip);

                        REQUEST_COUNTER.fetch_sub(1, Relaxed);
                        resp
                    }
                }))
            }
        }
        );


        tokio::spawn(async move {


            // let server = Server::builder(TlsAcceptor::new(tls_cfg, incoming)).serve(service);
            let server = Server::builder(TlsAcceptor::new(tls_cfg, incoming)).serve(service);
            // Run the future, keep going until an error occurs.
            info!("Starting to serve on https://{}.", addr);

            match server.await {
                Ok(_) => {}
                Err(e) => {
                    error!("error while starting tls socket: {:?}",e);
                }
            }
        });
        return Ok(());
    }

    // Load public certificate from file.
    fn load_certs(filename: &str) -> io::Result<Vec<rustls::Certificate>> {
        // Open certificate file.
        match File::open(filename) {
            Ok(file) => {
                let mut reader = io::BufReader::new(file);
                match rustls_pemfile::certs(&mut reader) {
                    Ok(certs) => {
                        let mut cert_list = vec![];
                        for x in certs {
                            cert_list.push(rustls::Certificate(x));
                        }
                        Ok(cert_list)
                    }
                    Err(e) => {
                        return Err(Error::new(io::ErrorKind::Other, format!("{:?}", e)));
                    }
                }
            }
            Err(e) => {
                return Err(Error::new(io::ErrorKind::Other, format!("failed to open file: {}: {:?}", filename, e)));
            }
        }
    }

    // Load private key from file.
    fn load_private_key(filename: &str) -> io::Result<rustls::PrivateKey> {
        return match File::open(filename) {
            Ok(file) => {
                let mut reader = io::BufReader::new(file);
                match rustls_pemfile::rsa_private_keys(&mut reader) {
                    // match rustls_pemfile::rsa_private_keys(&mut reader) {
                    Ok(rustls_native_certs) => {
                        if rustls_native_certs.len() != 1 {
                            return Err(Error::new(io::ErrorKind::Other, format!("expected a single private key in: {} found: {}", filename, rustls_native_certs.len())));
                        }
                        Ok(rustls::PrivateKey(rustls_native_certs[0].clone()))
                    }
                    Err(e) => {
                        return Err(Error::new(io::ErrorKind::Other, format!("failed to open file: {}: {:?}", filename, e)));
                    }
                }
            }
            Err(e) => {
                Err(Error::new(io::ErrorKind::Other, format!("failed to open file: {}: {:?}", filename, e)))
            }
        };
    }
}

pub struct TlsAcceptor {
    config: Arc<ServerConfig>,
    incoming: AddrIncoming,
}

impl TlsAcceptor {
    pub fn new(config: Arc<ServerConfig>, incoming: AddrIncoming) -> TlsAcceptor {
        TlsAcceptor { config, incoming }
    }
}

enum State {
    Handshaking(tokio_rustls::Accept<AddrStream>),
    Streaming(tokio_rustls::server::TlsStream<AddrStream>),
}

// tokio_rustls::server::TlsStream doesn't expose constructor methods,
// so we have to TlsAcceptor::accept and handshake to have access to it
// TlsStream implements AsyncRead/AsyncWrite handshaking tokio_rustls::Accept first
pub struct TlsStream {
    state: State,
}

impl TlsStream {
    fn new(stream: AddrStream, config: Arc<ServerConfig>) -> TlsStream {
        let accept = tokio_rustls::TlsAcceptor::from(config).accept(stream);
        TlsStream {
            state: State::Handshaking(accept),
        }
    }
}

impl AsyncRead for TlsStream {
    fn poll_read(
        self: Pin<&mut Self>,
        cx: &mut Context,
        buf: &mut ReadBuf,
    ) -> Poll<io::Result<()>> {
        let pin = self.get_mut();
        match pin.state {
            State::Handshaking(ref mut accept) => match ready!(Pin::new(accept).poll(cx)) {
                Ok(mut stream) => {
                    let result = Pin::new(&mut stream).poll_read(cx, buf);
                    pin.state = State::Streaming(stream);
                    result
                }
                Err(err) => Poll::Ready(Err(err)),
            },
            State::Streaming(ref mut stream) => Pin::new(stream).poll_read(cx, buf),
        }
    }
}

impl AsyncWrite for TlsStream {
    fn poll_write(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &[u8],
    ) -> Poll<io::Result<usize>> {
        let pin = self.get_mut();
        match pin.state {
            State::Handshaking(ref mut accept) => match ready!(Pin::new(accept).poll(cx)) {
                Ok(mut stream) => {
                    let result = Pin::new(&mut stream).poll_write(cx, buf);
                    pin.state = State::Streaming(stream);
                    result
                }
                Err(err) => Poll::Ready(Err(err)),
            },
            State::Streaming(ref mut stream) => Pin::new(stream).poll_write(cx, buf),
        }
    }

    fn poll_flush(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<io::Result<()>> {
        match self.state {
            State::Handshaking(_) => Poll::Ready(Ok(())),
            State::Streaming(ref mut stream) => Pin::new(stream).poll_flush(cx),
        }
    }

    fn poll_shutdown(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<io::Result<()>> {
        match self.state {
            State::Handshaking(_) => Poll::Ready(Ok(())),
            State::Streaming(ref mut stream) => Pin::new(stream).poll_shutdown(cx),
        }
    }
}

impl Accept for TlsAcceptor {
    type Conn = TlsStream;
    type Error = Error;

    fn poll_accept(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
    ) -> Poll<Option<Result<Self::Conn, Self::Error>>> {
        let pin = self.get_mut();
        match ready!(Pin::new(&mut pin.incoming).poll_accept(cx)) {
            Some(Ok(sock)) => Poll::Ready(Some(Ok(TlsStream::new(sock, pin.config.clone())))),
            Some(Err(e)) => Poll::Ready(Some(Err(e))),
            None => Poll::Ready(None),
        }
    }
}