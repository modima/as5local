use crate::db::filter::OP;
use crate::http::errors::AppError;
use hyper::{Body, Request};
use qstring::QString;
use std::{collections::HashMap, str};
use strum::IntoEnumIterator;

const FLAG_NUMERIC: u8 = 0x01;
const FLAG_CODES: &str = "n";
const ALPHA_OP_CODES: [&str; 7] = ["EQ", "LT", "LE", "GT", "GE", "SW", "OB"];

#[derive(Debug, Clone)]
pub struct Parameter {
    pub attribute_name: String,
    values: Vec<String>,
    pub flags: u8,
    pub operator: OP,
}

impl Parameter {
    pub fn new(encoded_name: &str, values: Vec<String>) -> Result<Parameter, AppError> {
        let suffix;
        let attribute_name;
        let mut operator = OP::EQ;
        let mut flags: u8 = 0;
        if let Some(pos) = encoded_name.rfind("__") {
            attribute_name = &encoded_name[..pos];
            suffix = encoded_name[pos + 2..].to_string();
            for (i, oc) in ALPHA_OP_CODES.iter().enumerate() {
                if suffix.starts_with(oc) {
                    operator = OP::iter().nth(i).unwrap();
                    break;
                }
            }
            for (i, c) in FLAG_CODES.chars().enumerate() {
                if suffix.contains(c) {
                    flags |= 1 << i;
                }
            }
        } else {
            attribute_name = encoded_name;
        }
        if attribute_name.len() == 0 {
            return Err(AppError::new(500, "empty parameter name"));
        }

        Ok(Parameter {
            attribute_name: attribute_name.to_string(),
            values: values.clone(),
            flags,
            operator,
        })
    }

    pub fn as_string(&self) -> String {
        if self.values.len() == 0 {
            "".to_string()
        } else {
            self.values[0].clone()
        }
    }

    pub fn as_bool(&self) -> bool {
        if self.values.len() == 0 {
            false
        } else {
            self.values[0].parse().unwrap_or(false)
        }
    }

    pub fn as_i32(&self) -> Result<i32, AppError> {
        if self.values.len() == 0 {
            Ok(0)
        } else {
            match self.values[0].parse::<f32>() {
                Ok(val) => Ok(val as i32),
                Err(e) => Err(e.into()),
            }
        }
    }

    pub fn as_index_values(&self) -> Result<Vec<Vec<u8>>, AppError> {
        let mut values: Vec<Vec<u8>> = Vec::new();
        for v in self.values.clone() {
            let value;
            if self.operator == OP::OB {
                value = v.as_bytes().to_vec();
            } else {
                if (self.flags & FLAG_NUMERIC) != 0 {
                    value = match v.parse::<f64>() {
                        Ok(n) => [[b'n'].to_vec(), n.to_be_bytes().to_vec()].concat(),
                        Err(_) => [[b'n'].to_vec(), 0_f64.to_be_bytes().to_vec()].concat()
                    }
                } else {
                    value = [[b's'].to_vec(), v.as_bytes().to_vec()].concat()
                }
            }
            values.push(value);
        }
        Ok(values)
    }

    pub fn is_reserved(&self) -> bool {
        self.attribute_name.starts_with("_") && self.attribute_name.ends_with("_")
    }
}

pub fn parse(req: &Request<Body>) -> Result<HashMap<String, Parameter>, AppError> {
    let params: HashMap<String, Vec<String>> = req
        .uri()
        .query()
        .map(|query| {
            let mut p = HashMap::new();
            let pairs = QString::from(query).into_pairs();
            for (key, val) in pairs {
                p.entry(key).or_insert(Vec::new()).push(val.to_string());
            }
            p
        })
        .unwrap_or_else(HashMap::new);
    let mut result: HashMap<String, Parameter> = HashMap::new();
    for (key, vals) in params.into_iter() {
        let p = Parameter::new(&key, vals)?;
        result.insert(key, p);
    }
    Ok(result)
}
