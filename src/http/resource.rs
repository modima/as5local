use crate::http::{errors::AppError, utils};
use bytes::Bytes;
use flate2::{
    read::{GzDecoder, GzEncoder},
    Compression,
};
use futures::stream;
use md5::{Digest, Md5};
use std::io::prelude::*;

const CHUNK_SIZE: usize = 32768;

#[derive(Debug)]
pub struct Resource {
    pub path: String,
    pub name: String,
    pub modified: String,
    pub data: Bytes,
    pub views: Bytes,
    pub version: i32,
    pub viewentry_ids: Vec<String>,
    pub encoding: Option<String>,
}

impl Resource {
    pub fn new(
        path: String,
        name: String,
        data: Bytes,
        views_opt: Option<Bytes>,
        version: i32,
        modified: String,
        viewentry_ids: Vec<String>,
        encoding: Option<String>,
        is_old: bool,
    ) -> Result<Resource, AppError> {
        let views = match views_opt {
            Some(v) => {
                if is_old {
                    let mut views_bytes = Vec::new();
                    let mut z = GzDecoder::new(&v[..]);
                    z.read_to_end(&mut views_bytes)?;
                    Bytes::from(views_bytes)
                } else {
                    v
                }
            }
            None => {
                // ectract views from resource (may fail if ressource is encrypted)
                let value: Result<serde_json::Value, _> = serde_json::from_slice(&data[..]);
                match value {
                    Ok(root) => match root.get("_views_") {
                        None => Bytes::from("[]".as_bytes()),
                        Some(views) => Bytes::from(serde_json::to_vec(views)?),
                    },
                    Err(e) => {
                        log::debug!("Views extraction failed: {}", e);
                        Bytes::from("[]".as_bytes())
                    }
                }
            }
        };
        Ok(Resource {
            path: path,
            name: name,
            data: data,
            views: views,
            version: version,
            modified: modified,
            viewentry_ids: viewentry_ids,
            encoding: encoding,
        })
    }
    pub fn as_stream(&self) -> impl stream::Stream<Item = Result<Bytes, std::convert::Infallible>> {
        let data = self.data.clone();
        let len = data.len();
        let chunks = (0..len).step_by(CHUNK_SIZE).map(move |x| {
            let range = x..len.min(x + CHUNK_SIZE);
            Result::<_, std::convert::Infallible>::Ok(data.slice(range))
        });
        log::debug!("chunks: {:?}", chunks);
        stream::iter(chunks)
    }

    pub fn data_as_gzip(&self) -> Result<Vec<u8>, AppError> {
        let mut result = Vec::new();
        let mut z = GzEncoder::new(&self.data[..], Compression::default());
        z.read_to_end(&mut result)?;
        Ok(result)
    }

    pub fn views_as_gzip(&self) -> Result<Vec<u8>, AppError> {
        let mut result = Vec::new();
        let mut z = GzEncoder::new(&self.views[..], Compression::default());
        z.read_to_end(&mut result)?;
        Ok(result)
    }

    pub fn path_as_hash(&self) -> String {
        format!(
            "{:x}",
            Md5::digest(&[self.path.as_bytes(), self.name.as_bytes(),].concat())
        )
        .to_string()
    }
}

const TYPE_FILE: u8 = 0;
const TYPE_DIR: u8 = 1;
const TYPE_INDEX: u8 = 2;

#[derive(Debug)]
pub struct ResourceKey {
    rtype: u8,
    pub dir: String,
    pub name: String,
    _parts: Vec<String>,
}

fn is_valid_key(key: &String) -> bool {
    return key.chars().all(|c| {
        char::is_ascii_alphanumeric(&c)
            || matches!(
                c,
                '$' | '-' | '_' | '.' | '+' | '!' | '*' | '\'' | '(' | ')' | ',' | '/' | ':'
            )
    });
}

impl ResourceKey {
    pub fn new(resource_key: &String) -> Result<ResourceKey, AppError> {
        let mut key = resource_key.clone();
        if !is_valid_key(&key) {
            return Err(AppError::new(500, "invalid character in resource key"));
        }

        let mut rtype = TYPE_FILE;
        if !key.starts_with("/") || key.starts_with("//") || key.ends_with("///") {
            return Err(AppError::new(500, "malformed resource key"));
        }
        if key.ends_with("//") {
            rtype = TYPE_INDEX;
            key.truncate(key.len() - 2);
        }
        if key.ends_with("/") {
            rtype = TYPE_DIR;
            key.truncate(key.len() - 1);
        }

        let mut parts: Vec<String> = Vec::new();
        while key.len() > 0 {
            // get next segment
            let part: String;
            match key.find('/') {
                Some(pos) => {
                    part = key[..pos].to_string();
                    key = key[pos + 1..].to_string();
                }
                _ => {
                    part = (*key).to_string();
                    key.clear();
                }
            }
            // check for path operations
            if part.len() == 0 || part == ".." {
                if parts.len() > 0 {
                    parts.pop();
                }
            } else if part == "." {
            } else {
                parts.push(part);
            }
        }

        log::debug!("rk parts: {:?}", parts);

        let mut dir = String::new();
        let mut name = String::new();
        for part in parts.iter() {
            dir.push_str(&name);
            dir.push_str("/");
            name = part.to_string();
        }

        Ok(ResourceKey {
            rtype,
            dir,
            name,
            _parts: parts,
        })
    }

    pub fn get_path(&self) -> String {
        let mut path = self.dir.clone();
        let delim = &"//".to_string()[..self.rtype as usize].to_string();
        path.push_str(&self.name);
        path.push_str(delim);
        path
    }

    pub fn is_file(&self) -> bool {
        return self.rtype == TYPE_FILE;
    }

    pub fn is_directory(&self) -> bool {
        return self.rtype == TYPE_DIR;
    }

    pub fn is_index(&self) -> bool {
        return self.rtype == TYPE_INDEX;
    }

    pub fn get_suffix(&self) -> String {
        if let Some(pos) = self.name.rfind('.') {
            self.name[pos + 1..].to_lowercase()
        } else {
            "".to_string()
        }
    }

    pub fn get_content_type(&self) -> String {
        utils::guess_content_type_from_extension(&self.get_suffix()).to_string()
    }

    pub fn path_as_hash(&self) -> String {
        format!(
            "{:x}",
            Md5::digest(&[self.dir.as_bytes(), self.name.as_bytes(),].concat())
        )
        .to_string()
    }
}
