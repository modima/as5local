use crate::http::errors::AppError;
use bytes::Bytes;
use flate2::{read::GzEncoder, Compression};
use futures::stream;
use serde::Serialize;
use std::io::prelude::*;

const CHUNK_SIZE: usize = 32768;

#[derive(Serialize)]
pub struct Record {
    pub _name_: String,
}

#[derive(Serialize)]
pub struct ResultSet {
    pub _results_: Vec<Record>,
    pub _count_: i32,
    #[serde(skip_serializing_if = "String::is_empty")]
    pub _cursor_: String,
}

impl ResultSet {
    pub fn new() -> ResultSet {
        ResultSet {
            _results_: Vec::<Record>::new(),
            _count_: 0,
            _cursor_: "".to_string(),
        }
    }

    pub fn add_result(&mut self, result: Record) {
        self._results_.push(result);
        self._count_ = self._results_.len() as i32;
    }

    pub fn as_stream(
        &self,
        zipped: bool,
    ) -> Result<impl stream::Stream<Item = Result<Bytes, std::convert::Infallible>>, AppError> {
        let json_data = serde_json::to_vec(self).unwrap();
        let data = if zipped {
            let mut result = Vec::new();
            let mut z = GzEncoder::new(&json_data[..], Compression::default());
            z.read_to_end(&mut result)?;
            Bytes::from(result)
        } else {
            Bytes::from(json_data)
        };
        let len = data.len();
        let chunks = (0..len).step_by(CHUNK_SIZE).map(move |x| {
            let range = x..len.min(x + CHUNK_SIZE);
            Result::<_, std::convert::Infallible>::Ok(data.slice(range))
        });
        log::debug!("chunks: {:?}", chunks);
        Ok(stream::iter(chunks))
    }
}
