use crate::http::errors::AppError;
use std::str;

pub fn parse_version_tag(vt: &str) -> i32 {
    let mut tag = vt.to_string();
    if tag.len() == 0 {
        return -1;
    }
    tag = tag.replace("W/", "");
    if tag == "*" {
        return 0;
    }
    tag.parse().unwrap_or(-1)
}

pub fn guess_content_type_from_extension(extension: &String) -> String {
    let ct;
    match extension.as_str() {
        "" | "json" => ct = "application/json",
        "html" => ct = "text/html",
        "js" => ct = "text/javascript",
        "ts" => ct = "text/typescript",
        "xml" => ct = "text/xml",
        "gif" => ct = "image/gif",
        "jpg" => ct = "image/jpeg",
        "png" => ct = "image/png",
        "css" => ct = "text/css",
        "zip" => ct = "application/zip",
        "gz" | "gzip" => ct = "application/x-gzip",
        "mp3" => ct = "audio/mpeg3",
        "mp4" => ct = "video/mp4",
        "avi" => ct = "video/avi",
        "wav" => ct = "audio/vnd.wave",
        "csv" => ct = "text/csv",
        "txt" => ct = "text/plain",
        "pdf" => ct = "application/pdf",
        "svg" => ct = "image/svg+xml",
        "eot" => ct = "application/vnd.ms-fontobject",
        "woff" => ct = "application/x-woff",
        "otf" => ct = "font/otf",
        "ttf" => ct = "font/ttf",
        "vcard" | "vcf" => ct = "text/vcard",
        "md" => ct = "text/markdown",
        _ => ct = "application/octet-stream",
    }

    log::debug!(
        "guess_content_type_from_extension: ext: {} ct: {}",
        extension,
        ct
    );
    ct.to_string()
}

pub fn guess_charset_from_content_type(content_type: &String) -> String {
    let mut ct = "";
    if content_type.trim().is_empty() {
    } else if content_type.starts_with("text/") {
        ct = "UTF-8";
    } else if content_type.ends_with("json") {
        ct = "UTF-8";
    }
    ct.to_string()
}

pub fn get_default_status_text(statuscode: u16) -> String {
    match statuscode {
        100 => String::from("Continue"),
        101 => String::from("Switching Protocols"),
        200 => String::from("OK"),
        201 => String::from("Created"),
        202 => String::from("Accepted"),
        203 => String::from("Non-Authoritative Information"),
        204 => String::from("No Content"),
        205 => String::from("Reset Content"),
        206 => String::from("Partial Content"),
        300 => String::from("Multiple Choices"),
        301 => String::from("Moved Permanently"),
        302 => String::from("Found"),
        303 => String::from("See Other"),
        304 => String::from("Not Modified"),
        305 => String::from("Use Proxy"),
        307 => String::from("Temporary Redirect"),
        400 => String::from("Bad Request"),
        401 => String::from("Unauthorized"),
        402 => String::from("Payment Required"),
        403 => String::from("Forbidden"),
        404 => String::from("Not Found"),
        405 => String::from("Method Not Allowed"),
        406 => String::from("Not Acceptable"),
        407 => String::from("Proxy Authentication Required"),
        408 => String::from("Request Timeout"),
        409 => String::from("Conflict"),
        410 => String::from("Gone"),
        411 => String::from("Length Required"),
        412 => String::from("Precondition Failed"),
        413 => String::from("Request Entity Too Large"),
        414 => String::from("Request-URI Too Long"),
        415 => String::from("Unsupported Media Type"),
        416 => String::from("Requested Range Not Satisfiable"),
        417 => String::from("Expectation Failed"),
        500 => String::from("Internal Server Error"),
        501 => String::from("Not Implemented"),
        502 => String::from("Bad Gateway"),
        503 => String::from("Service Unavailable"),
        504 => String::from("Gateway Timeout"),
        505 => String::from("HTTP Version Not Supported"),
        _ => String::from("(Reserved Status Code)"),
    }
}

pub fn vec_string_as_bytes(input: Vec<&str>) -> Vec<u8> {
    let mut result = Vec::new();
    for (i, v) in input.iter().enumerate() {
        result.extend(v.as_bytes());
        if i < input.len() - 1 {
            result.push(b'\0');
        }
    }
    result
}

pub fn bytes_as_vec_string(input: Vec<u8>) -> Result<Vec<String>, AppError> {
    let mut result = Vec::new();
    let splits = input.split(|i| *i == b'\0');
    for s in splits {
        result.push(str::from_utf8(&s.to_vec())?.to_string());
    }
    Ok(result)
}

pub fn get_range_limit(s: &str) -> String {
    if s.len() == 0 {
        return s.to_string();
    }
    let pos = s.len() - 1;
    let mut r = s[..pos].to_string();
    let mut c = s.chars().nth(pos).unwrap();
    log::debug!("get_range_limit last char: {}", c);
    c = (c as u8 + 1) as char;
    log::debug!("get_range_limit inc char: {}", c);
    r.push(c as char);
    log::debug!("limit: {}", r);
    r
}
