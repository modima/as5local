use crate::http::{errors::AppError, resource::Resource, results::ResultSet, utils};
use crate::index::results::IndexResultSet;
use hyper;
use std::collections::HashMap;

pub struct Response<'a> {
    pub namesonly: bool,
    pub showindex: bool,
    pub include_index_values: bool,
    pub results: Option<&'a ResultSet>,
    pub index_results: Option<&'a IndexResultSet>,
    pub resource: Option<&'a Resource>,
    pub status: u16,
    pub statustext: String,
    pub headers: HashMap<&'a str, &'a str>,
}

impl<'a> Response<'a> {
    pub fn new() -> Response<'a> {
        Response {
            namesonly: false,
            showindex: false,
            include_index_values: true,
            results: None,
            resource: None,
            index_results: None,
            status: 204,
            statustext: "".to_string(),
            headers: HashMap::new(),
        }
    }

    pub fn remove_header(&mut self, key: &'a str) -> &mut Self {
        self.headers.remove(key);
        self
    }
    pub fn add_header(&mut self, key: &'a str, value: &'a str) -> &mut Self {
        self.headers.insert(key, value);
        self
    }

    pub fn get_statustext(&self) -> String {
        if self.statustext.len() > 0 {
            self.statustext.clone()
        } else {
            utils::get_default_status_text(self.status)
        }
    }

    pub fn set_status(&'a mut self, status: u16) -> &mut Self {
        self.status = status;
        self
    }

    pub fn set_resource(&mut self, resource: &'a Resource) -> &mut Self {
        self.resource = Some(resource);
        self
    }

    pub fn set_results(&mut self, results: &'a ResultSet) -> &mut Self {
        self.results = Some(results);
        self
    }

    pub fn set_index_results(
        &mut self,
        index_results: &'a IndexResultSet,
        namesonly: bool,
        showindex: bool,
        include_index_values: bool,
    ) -> &mut Self {
        self.namesonly = namesonly;
        self.showindex = showindex;
        self.include_index_values = include_index_values;
        self.index_results = Some(index_results);
        self
    }

    pub fn set_error(&mut self, e: &AppError) -> &mut Self {
        self.status = e.status;
        self.statustext = e.message.clone();
        self
    }

    pub fn build(&mut self) -> Result<hyper::Response<hyper::Body>, hyper::http::Error> {
        let mut response = hyper::Response::builder().status(self.status);
        for (key, value) in self.headers.iter() {
            log::debug!("add header {}: {}", key, value);
            let h_name = hyper::header::HeaderName::from_bytes(key.as_bytes()).unwrap();
            let h_value = hyper::header::HeaderValue::from_bytes(value.as_bytes()).unwrap();
            response.headers_mut().unwrap().insert(h_name, h_value);
        }
        if let Some(results) = self.results {
            match results.as_stream(true) {
                Ok(stream) => response
                    .status(200)
                    .header("Content-Encoding", "gzip")
                    .body(hyper::Body::wrap_stream(stream)),
                Err(e) => response
                    .header("Content-Type", "text/plain;charset=utf-8")
                    .body(hyper::Body::from(format!("{} - {}", 500, e.message,))),
            }
        } else if let Some(index_results) = self.index_results {
            match index_results.as_stream(self.namesonly, self.showindex, self.include_index_values, true) {
                Ok(stream) => response
                    .status(200)
                    .header("Content-Encoding", "gzip")
                    .body(hyper::Body::wrap_stream(stream)),
                Err(e) => response
                    .header("Content-Type", "text/plain;charset=utf-8")
                    .body(hyper::Body::from(format!("{} - {}", 500, e.message,))),
            }
        } else {
            match self.resource {
                Some(resource) => {
                    if let Some(enc) = &resource.encoding {
                        response.headers_mut().unwrap().insert(
                            "X-Encoding",
                            hyper::header::HeaderValue::from_bytes(enc.as_bytes()).unwrap(),
                        );
                    }
                    response
                        .header("Etag", format!("W/{}", resource.version))
                        .header("Content-Encoding", "gzip")
                        .body(hyper::Body::wrap_stream(resource.as_stream()))
                }
                None => {
                    response.headers_mut().unwrap().insert(
                        "Content-Type",
                        hyper::header::HeaderValue::from_bytes(
                            "text/plain;charset=utf-8".as_bytes(),
                        )
                        .unwrap(),
                    );
                    response.body(hyper::Body::from(format!(
                        "{} - {}",
                        self.status,
                        self.get_statustext()
                    )))
                }
            }
        }
    }
}
