use sqlx;
use std::{fmt, io};

#[derive(Debug)]
pub struct AppError {
    pub status: u16,
    pub message: String,
}

impl AppError {
    pub fn new(status: u16, msg: &str) -> AppError {
        AppError {
            status: status,
            message: msg.to_string(),
        }
    }
}

impl fmt::Display for AppError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} - {}", self.status, self.message)
    }
}

impl From<io::Error> for AppError {
    fn from(error: io::Error) -> Self {
        AppError::new(500, &format!("io error: {}", error.to_string()))
    }
}

impl From<sqlx::Error> for AppError {
    fn from(error: sqlx::Error) -> Self {
        AppError::new(500, &format!("database error: {}", error.to_string()))
    }
}

impl From<&dyn sqlx::error::DatabaseError> for AppError {
    fn from(error: &dyn sqlx::error::DatabaseError) -> Self {
        AppError::new(500, &format!("database error: {}", error.to_string()))
    }
}

impl From<serde_json::Error> for AppError {
    fn from(error: serde_json::Error) -> Self {
        AppError::new(500, &format!("json error: {}", error.to_string()))
    }
}

impl From<std::str::Utf8Error> for AppError {
    fn from(error: std::str::Utf8Error) -> Self {
        AppError::new(500, &format!("utf8 decode error: {}", error.to_string()))
    }
}

impl From<base64::DecodeError> for AppError {
    fn from(error: base64::DecodeError) -> Self {
        AppError::new(500, &format!("base64 decode error: {}", error.to_string()))
    }
}

impl From<multer::Error> for AppError {
    fn from(error: multer::Error) -> Self {
        AppError::new(500, &format!("multipart error: {}", error.to_string()))
    }
}

impl From<rocksdb::Error> for AppError {
    fn from(error: rocksdb::Error) -> Self {
        AppError::new(500, &format!("rocksdb error: {}", error.to_string()))
    }
}

impl From<std::num::ParseIntError> for AppError {
    fn from(error: std::num::ParseIntError) -> Self {
        AppError::new(500, &format!("parse error: {}", error.to_string()))
    }
}

impl From<std::num::ParseFloatError> for AppError {
    fn from(error: std::num::ParseFloatError) -> Self {
        AppError::new(500, &format!("parse error: {}", error.to_string()))
    }
}

impl From<std::array::TryFromSliceError> for AppError {
    fn from(error: std::array::TryFromSliceError) -> Self {
        AppError::new(500, &format!("parse error: {}", error.to_string()))
    }
}

impl From<hyper::Error> for AppError {
    fn from(error: hyper::Error) -> Self {
        AppError::new(500, &format!("hyper error: {}", error.to_string()))
    }
}
