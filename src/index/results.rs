use crate::http::errors::AppError;
use bytes::Bytes;
use flate2::{
    read::{GzDecoder, GzEncoder},
    Compression,
};
use futures::stream;
use itertools::Itertools;
use serde::Serialize;
use serde_json::{json, Map, Value};
use std::cmp::Ordering;
use std::hash::{Hash, Hasher};
use std::{collections::HashMap, convert::TryFrom, io::prelude::*, str};
use base64::Engine;

const CHUNK_SIZE: usize = 4096;

#[derive(Debug, Clone, Serialize)]
struct HashableValue<'a>(&'a Value);

impl<'a> PartialEq<Self> for HashableValue<'a> {
    fn eq(&self, other: &Self) -> bool {
        self.0.eq(other.0)
    }
}

impl<'a> Eq for HashableValue<'a> {}

impl<'a> PartialOrd for HashableValue<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
impl<'a> Ord for HashableValue<'a> {
    fn cmp(&self, other: &Self) -> Ordering {
        let o1 = serde_json::to_vec(self.0).unwrap();
        let o2 = serde_json::to_vec(other.0).unwrap();
        o1.cmp(&o2)
    }
}

impl<'a> From<&'a Value> for HashableValue<'a> {
    fn from(v: &'a Value) -> HashableValue<'a> {
        HashableValue(v)
    }
}

impl<'a> Hash for HashableValue<'a> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match &self.0 {
            Value::Null => 0.hash(state),
            Value::Bool(b) => b.hash(state),
            Value::Number(n) => n.hash(state),
            Value::String(str) => str.hash(state),
            Value::Array(arr) => arr.iter().for_each(|a| HashableValue(a).hash(state)),
            Value::Object(obj) => obj.iter().for_each(|entry| {
                entry.0.hash(state);
                HashableValue(entry.1).hash(state);
            }),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct IndexResult {
    pub name: String,
    pub viewentry_id: String,
    pub columns: HashMap<String, Vec<Vec<u8>>>,
    pub viewentry_ids: Option<Vec<String>>,
    pub views: Option<Vec<u8>>,
}

#[derive(Debug)]
pub struct IndexResultSet {
    results: Vec<IndexResult>,
    cursor: String,
}

impl IndexResultSet {
    pub fn new(results: Vec<IndexResult>, cursor: &[u8]) -> IndexResultSet {
        IndexResultSet {
            results,
            cursor: base64::engine::general_purpose::URL_SAFE_NO_PAD.encode(cursor),
        }
    }

    pub fn to_keysonly_result(
        &self,
        include_index_values: bool,
        showindex: bool,
    ) -> Result<Vec<u8>, AppError> {
        let mut results = Vec::new();
        let mut iter = self.results.iter().peekable();
        let mut last_entry = Vec::new();
        while let Some(row) = iter.next() {
            //log::debug!("row: {:?}", row);
            let mut record = Map::new();
            record.insert("_name_".to_string(), json!(row.name));
            if !include_index_values {
                results.push(json!(record));
                continue;
            }
            let mut indexed_fields = Vec::new();
            for (key, values) in row.columns.iter() {
                for val in values {
                    // skip duplicates (same viewentry and same value)
                    if last_entry.eq(&[row.viewentry_id.as_bytes(), val].concat()) {
                        continue;
                    }
                    last_entry = [row.viewentry_id.as_bytes(), val].concat();
                    //log::debug!("val: {:?}", val);
                    if !key.eq("_dummy_") {
                        let rtype = val[0] as char;
                        let rval = &val[1..];
                        let json_val = match rtype {
                            's' => {
                                let val_str = str::from_utf8(rval).unwrap();
                                json!(val_str)
                            }
                            'n' => {
                                let val_num;
                                match <[u8; 8]>::try_from(rval) {
                                    Ok(n) => val_num = f64::from_be_bytes(n),
                                    Err(e) => return Err(e.into()),
                                };
                                json!(val_num)
                            }
                            _ => json!(null),
                        };
                        if showindex {
                            indexed_fields.push(key);
                        }
                        record.insert(key.clone(), json_val);
                    }
                    if showindex {
                        record.insert("_indexed_".to_string(), json!(indexed_fields));
                    }
                    //log::debug!("add result {:?}", record);
                    results.push(json!(record));
                }
            }
        }
        log::debug!("RESULTS: {:?}", results);
        let sorted_results = results
            .iter()
            .map(HashableValue)
            //.sorted()
            .collect::<Vec<HashableValue>>();
        //log::debug!("SORTED RESULTS: {:?}", sorted_results);
        self.to_json(&sorted_results)
    }

    pub fn to_full_result(&self, showindex: bool) -> Result<Vec<u8>, AppError> {
        let mut results = Vec::new();
        let mut iter = self.results.iter().peekable();
        let mut processed_veids = HashMap::new(); // deduplication of view entries
        while let Some(row) = iter.next() {
            let viewentry_ids = match &row.viewentry_ids {
                None => Vec::new(),
                Some(ids) => ids.clone(),
            };
            let mut views_bytes = Vec::new();
            match &row.views {
                None => {
                    log::debug!("no views found");
                    views_bytes = vec!['[' as u8, ']' as u8];
                }
                Some(views) => {
                    log::debug!("views found");
                    let mut z = GzDecoder::new(&views[..]);
                    z.read_to_end(&mut views_bytes)?;
                }
            };
            let views: Value = serde_json::from_slice(&views_bytes)?;
            let views_array = views.as_array().unwrap();
            if views_array.len() == 0 {
                continue;
            }
            //log::debug!("viewentry_ids: {:?}", viewentry_ids);
            //log::debug!("row: {:?}", row);
            let view_idx = viewentry_ids
                .iter()
                .position(|id| id.eq(&row.viewentry_id))
                .unwrap();
            let viewentry = &views_array[view_idx];
            let orig_view_obj = viewentry.as_object().unwrap();
            let mut clean_view_obj = Map::new();
            let mut indexed_fields = Vec::new();
            clean_view_obj.insert("_name_".to_string(), json!(row.name));
            for (key, val) in orig_view_obj.into_iter() {
                if !key.eq("_viewname_") {
                    let is_indexed = key.ends_with("__i");
                    let new_key = if is_indexed {
                        key[..key.len() - "__i".len()].to_string()
                    } else {
                        key.to_string()
                    };
                    if showindex && is_indexed {
                        indexed_fields.push(new_key.clone());
                    }
                    if val.is_boolean() {
                        if val.as_bool().unwrap() {
                            clean_view_obj.insert(new_key, json!(1_f64));
                        } else {
                            clean_view_obj.insert(new_key, json!(0_f64));
                        };
                    } else if val.is_number() {
                        clean_view_obj.insert(new_key, json!(val.as_f64()));
                    } else if val.is_object() {
                        clean_view_obj.insert(new_key, json!(serde_json::to_string(val).unwrap()));
                    } else if val.is_array() {
                        let sorted_list = val
                            .as_array()
                            .unwrap()
                            .iter()
                            .map(HashableValue)
                            .sorted()
                            .unique()
                            .collect::<Vec<HashableValue>>();
                        clean_view_obj.insert(new_key, json!(sorted_list));
                    } else {
                        log::debug!("add result: {}", val);
                        clean_view_obj.insert(new_key, val.clone());
                    }
                }
            }
            if showindex {
                clean_view_obj.insert("_indexed_".to_string(), json!(indexed_fields));
            }
            for (_key, values) in row.columns.iter() {
                for _val in values {
                    if !processed_veids.contains_key(&row.viewentry_id) {
                        processed_veids.insert(&row.viewentry_id, true);
                        //results.push(json!(clean_view_obj));
                        log::debug!("VIEW IDX POS {} -- {}", row.viewentry_id, view_idx);
                        results.push((view_idx, json!(clean_view_obj)));
                    }
                }
            }
        }
        /*
        let sorted_results = results
            .iter()
            .map(HashValue)
            .sorted()
            .unique()
            .collect::<Vec<HashValue>>();
        */
        // sort by viewentry maybe not needed, because also no consistent sorting in AS5 (remove to increase performance)
        log::debug!("RESULTS: {:?}", results);
        //results.sort_by(|a, b| a.0.cmp(&b.0));
        let sorted_results = results
            .iter()
            .map(|a| HashableValue(&a.1))
            .collect::<Vec<HashableValue>>();
        log::debug!("SORTED RESULTS: {:?}", sorted_results);
        self.to_json(&sorted_results)
    }

    pub fn as_stream(
        &self,
        namesonly: bool,
        showindex: bool,
        include_index_values: bool,
        zipped: bool,
    ) -> Result<impl stream::Stream<Item = Result<Bytes, std::convert::Infallible>>, AppError> {
        let json_data = if namesonly {
            self.to_keysonly_result(include_index_values, showindex)?
        } else {
            self.to_full_result(showindex)?
        };
        let data = if zipped {
            let mut result = Vec::new();
            let mut z = GzEncoder::new(&json_data[..], Compression::default());
            z.read_to_end(&mut result)?;
            Bytes::from(result)
        } else {
            Bytes::from(json_data)
        };
        let len = data.len();
        let chunks = (0..len).step_by(CHUNK_SIZE).map(move |x| {
            let range = x..len.min(x + CHUNK_SIZE);
            Result::<_, std::convert::Infallible>::Ok(data.slice(range))
        });
        log::debug!("chunks: {:?}", chunks);
        Ok(stream::iter(chunks))
    }

    fn to_json(&self, results: &Vec<HashableValue>) -> Result<Vec<u8>, AppError> {
        let mut json_result = Map::new();
        json_result.insert("_count_".to_string(), json!(results.len()));
        if self.cursor.len() > 0 {
            json_result.insert("_cursor_".to_string(), json!(self.cursor));
        }
        json_result.insert("_results_".to_string(), json!(results));
        Ok(serde_json::to_vec(&json_result)?)
    }
}
