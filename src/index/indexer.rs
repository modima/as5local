use crate::index::record::{IndexRecord, ViewEntry};
use linked_hash_set::LinkedHashSet;
use md5::{Digest, Md5};
use std::{
    cmp::Reverse,
    collections::{HashMap, HashSet},
    sync::{Arc, RwLock},
};

#[derive(Debug)]
pub struct IndexRefreshResultHolder<'a> {
    pub putlist: Vec<&'a IndexRecord>,
    pub dellist: Vec<&'a IndexRecord>,
}

#[derive(Debug)]
struct CountEntry {
    old_viewentry: Arc<RwLock<ViewEntry>>,
    new_viewentry: Arc<RwLock<ViewEntry>>,
    count: u8,
}

fn match_rows(rows: &Vec<(Vec<u8>, Arc<RwLock<ViewEntry>>)>) -> Vec<CountEntry> {
    let mut counter: HashMap<String, CountEntry> = HashMap::new();
    let mut processed = HashSet::new();
    let mut skip_count = 0;
    let len = rows.len();
    for i in 0..len {
        if i == len - 1 {
            break;
        }
        if skip_count > 0 {
            skip_count -= 1;
            continue;
        }

        let r = if i < len - 2 {
            rows[i..i + 3].as_ref()
        } else {
            rows[i..i + 2].as_ref()
        };

        // match hash(path,viewname,fieldname,fieldvalue)?
        if r[0].0 != r[1].0 {
            processed.insert(&r[0].0);
            continue;
        }

        // sequence of old and new row?
        if r[0].1.read().unwrap().is_old == r[1].1.read().unwrap().is_old {
            processed.insert(&r[0].0);
            processed.insert(&r[1].0);
            skip_count = 1;
            continue;
        }

        // no third row with same hash?
        if r.len() == 3 && r[0].0 == r[2].0 {
            processed.insert(&r[0].0);
            processed.insert(&r[1].0);
            processed.insert(&r[2].0);
            skip_count = 2;
            continue;
        }

        // hash of row already processed?
        if processed.contains(&r[0].0) {
            continue;
        }

        // hash of next row already processed?
        if processed.contains(&r[1].0) {
            skip_count = 1;
            continue;
        }

        let new_viewentry;
        let old_viewentry;
        if r[0].1.read().unwrap().is_old {
            new_viewentry = r[1].1.clone();
            old_viewentry = r[0].1.clone();
        } else {
            new_viewentry = r[0].1.clone();
            old_viewentry = r[1].1.clone();
        };

        counter
            .entry(format!(
                "{}{}",
                old_viewentry.read().unwrap().id,
                new_viewentry.read().unwrap().id
            ))
            .or_insert(CountEntry {
                old_viewentry: old_viewentry.clone(),
                new_viewentry: new_viewentry.clone(),
                count: 0,
            })
            .count += 1;
        processed.insert(&r[0].0);
        skip_count = 1;
    }
    counter.into_iter().map(|(_key, entry)| entry).collect()
}

fn create_result<'a>(
    new_rows: &'a mut Vec<IndexRecord>,
    old_rows: &'a mut Vec<IndexRecord>,
    result: &mut IndexRefreshResultHolder<'a>,
) -> LinkedHashSet<String> {
    let mut new_viewentry_ids = LinkedHashSet::new();
    let mut i_old = 0;
    let mut i_new = 0;
    loop {
        if i_new == new_rows.len() {
            // delete all old rows without match
            for row in old_rows.iter().skip(i_old) {
                result.dellist.push(row);
            }
            break;
        } else if i_old == old_rows.len() {
            for row in new_rows.iter().skip(i_new) {
                // add all new rows without match
                result.putlist.push(row);
                new_viewentry_ids.insert(row.viewentry.read().unwrap().id.to_string());
            }
            break;
        }

        let new_row = &new_rows[i_new];
        let old_row = &old_rows[i_old];
        if new_row == old_row {
            let new_ve = new_row.viewentry.clone();
            let old_ve = old_row.viewentry.clone();

            // match values and viewentry ids --> ignore rows
            if new_row.fieldvalue == old_row.fieldvalue
                && new_ve.read().unwrap().id == old_ve.read().unwrap().id
            {
                new_viewentry_ids.insert(new_ve.read().unwrap().id.to_string());
                i_new += 1;
                i_old += 1;
            } else {
                // rows changed --> delete old and add new rows
                result.dellist.push(old_row);
                result.putlist.push(new_row);
                new_viewentry_ids.insert(new_ve.read().unwrap().id.to_string());
                i_new += 1;
                i_old += 1;
            }
        } else {
            if new_row < old_row {
                // singe new row --> insert
                result.putlist.push(new_row);
                new_viewentry_ids.insert(new_row.viewentry.read().unwrap().id.to_string());
                i_new += 1;
            } else {
                // singe old row --> delete
                result.dellist.push(old_row);
                i_old += 1;
            }
        }
    }
    new_viewentry_ids
}

pub fn refresh_indeces<'a>(
    new_rows: &'a mut Vec<IndexRecord>,
    old_rows_opt: Option<&'a mut Vec<IndexRecord>>,
    result: &mut IndexRefreshResultHolder<'a>,
) -> Vec<String> {
    let mut new_viewentry_ids = LinkedHashSet::new();
    match old_rows_opt {
        None => {
            for row in new_rows.iter_mut() {
                result.putlist.push(row);
                new_viewentry_ids.insert(row.viewentry.read().unwrap().id.to_string());
            }
        }
        Some(old_rows) => {
            let mut all_rows: Vec<(Vec<u8>, Arc<RwLock<ViewEntry>>)> = Vec::new();
            for (new, old) in new_rows.iter_mut().zip(old_rows.iter_mut()) {
                all_rows.push((
                    [
                        &Md5::digest(
                            &[
                                new.path.as_bytes(),
                                new.viewname.as_bytes(),
                                new.fieldname.as_bytes(),
                            ]
                            .concat(),
                        )
                        .as_slice(),
                        &new.fieldvalue[..],
                    ]
                    .concat(),
                    new.viewentry.clone(),
                ));
                all_rows.push((
                    [
                        &Md5::digest(
                            &[
                                old.path.as_bytes(),
                                old.viewname.as_bytes(),
                                old.fieldname.as_bytes(),
                            ]
                            .concat(),
                        )
                        .as_slice(),
                        &old.fieldvalue[..],
                    ]
                    .concat(),
                    old.viewentry.clone(),
                ))
            }
            // sort by hash/fieldvalue
            all_rows.sort_by(|x, y| x.0.cmp(&y.0));

            // count matching rows (exactly one old and one new)
            // --> sort by count descending
            let mut counter = match_rows(&all_rows);
            counter.sort_by_key(|c| Reverse(c.count));
            log::debug!("counter: {:?}", counter);

            // replace new ID with old ID
            for ce in counter.into_iter() {
                let mut new_viewentry = ce.new_viewentry.write().unwrap();
                let old_viewentry = ce.old_viewentry.read().unwrap();
                if !new_viewentry.was_replaced {
                    log::debug!("replace: {} -> {}", new_viewentry.id, old_viewentry.id);
                    new_viewentry.id.replace_range(.., &old_viewentry.id);
                    new_viewentry.was_replaced = true;
                }
            }
            new_viewentry_ids = create_result(new_rows, old_rows, result);
        }
    }
    new_viewentry_ids.into_iter().collect()
}
