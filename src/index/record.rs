use crate::http::{
    errors::AppError,
    resource::{Resource, ResourceKey},
};
use std::{
    cmp::Ordering,
    collections::hash_map::DefaultHasher,
    hash::{Hash, Hasher},
    str,
    sync::{Arc, RwLock},
};
use uuid::Uuid;

const INDEX_SUFFIX: &str = "__i";
const TYPE_PROPERTY: &str = "_viewname_";

const INDEX_TYPE_STRING: u8 = b's';
const INDEX_TYPE_NUMBER: u8 = b'n';

#[derive(Debug, Eq, Ord, PartialEq, PartialOrd)]
pub struct ViewEntry {
    pub id: String,
    pub is_old: bool,
    pub was_replaced: bool,
}

#[derive(Debug)]
pub struct IndexRecord {
    pub is_old_idx: bool, // is_old_idx row or new row // maybe not needed
    pub path: String,
    pub viewname: String,
    pub fieldname: String,
    pub fieldvalue: Vec<u8>,               // max 1501 bytes
    pub name: String,                      // resource name
    pub viewentry: Arc<RwLock<ViewEntry>>, // ref of corresponding view entry
}

fn create_hash<T>(obj: T) -> u64
where
    T: Hash,
{
    let mut hasher = DefaultHasher::new();
    obj.hash(&mut hasher);
    hasher.finish()
}

impl Hash for IndexRecord {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.path.hash(state);
        self.viewname.hash(state);
        self.fieldname.hash(state);
    }
}

impl Eq for IndexRecord {}

impl PartialEq for IndexRecord {
    fn eq(&self, other: &Self) -> bool {
        create_hash(self) == create_hash(other)
    }
}

impl PartialOrd for IndexRecord {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        create_hash(self).partial_cmp(&(create_hash(other)))
    }
}

impl Ord for IndexRecord {
    fn cmp(&self, other: &Self) -> Ordering {
        create_hash(self).cmp(&(create_hash(other)))
    }
}

impl IndexRecord {
    pub fn new(
        path: String,
        viewname: String,
        fieldname: String,
        fieldvalue: Vec<u8>,
        rname: String,
        viewentry: Arc<RwLock<ViewEntry>>,
        is_old_idx: bool,
    ) -> IndexRecord {
        IndexRecord {
            path: path,
            viewname: viewname,
            fieldname: fieldname,
            fieldvalue: fieldvalue,
            name: rname,
            viewentry: viewentry,
            is_old_idx: is_old_idx,
        }
    }

    pub fn key_as_bytes(&self) -> Vec<u8> {
        [
            self.path.as_bytes(),
            &[b'~'],
            self.viewname.as_bytes(),
            &[b'\0'],
            self.fieldname.as_bytes(),
            &[b'\0'],
            &self.fieldvalue,
            &[b'\0'],
            self.name.as_bytes(),
            &[b'\0'],
            self.viewentry.read().unwrap().id.as_bytes(),
        ]
        .concat()
    }
}

pub struct IndexSet {
    pub entries: Vec<Arc<RwLock<ViewEntry>>>,
    pub rows: Vec<IndexRecord>,
}

impl IndexSet {
    pub fn new(
        rkey: &ResourceKey,
        rsrc: &mut Resource,
        is_old: bool,
    ) -> Result<IndexSet, AppError> {
        let mut index_set = IndexSet {
            entries: Vec::new(),
            rows: Vec::new(),
        };
        let views: serde_json::Value = serde_json::from_slice(&rsrc.views)?;
        for view in views.as_array().unwrap() {
            log::debug!("view: {}", view);
            let viewentry_id = if is_old {
                rsrc.viewentry_ids.remove(0)
            } else {
                Uuid::new_v4().as_simple().to_string()
            };
            let ve = ViewEntry {
                id: viewentry_id,
                is_old: is_old,
                was_replaced: false,
            };
            let viewentry = Arc::new(RwLock::new(ve));
            let mut idx_rows = Vec::new();
            let viewname;
            match view.get(TYPE_PROPERTY) {
                Some(name) => viewname = name.as_str().unwrap(),
                None => viewname = "default",
            }
            for (k, v) in view.as_object().unwrap() {
                // only indexed values are relevant
                if !k.ends_with(INDEX_SUFFIX) {
                    continue;
                }
                let key = k[..k.len() - INDEX_SUFFIX.len()].to_string();
                let rows = create_rows(viewname, &key, &v, is_old, rkey, viewentry.clone())?;
                idx_rows.extend(rows);
            }
            // Add dummy row (required for view listing)
            let dummy = create_row(
                viewname,
                "_dummy_",
                "".as_bytes(),
                is_old,
                rkey,
                viewentry.clone(),
            )?;
            idx_rows.push(dummy);
            index_set.rows.extend(idx_rows);
            index_set.entries.push(viewentry);
        }
        Ok(index_set)
    }
}

fn create_rows(
    viewname: &str,
    key: &str,
    v: &serde_json::value::Value,
    is_old_idx: bool,
    rkey: &ResourceKey,
    viewentry: Arc<RwLock<ViewEntry>>,
) -> Result<Vec<IndexRecord>, AppError> {
    let mut index_rows = Vec::new();
    if v.is_null() {
        let value = [INDEX_TYPE_STRING];
        let row = create_row(viewname, &key, &value, is_old_idx, rkey, viewentry)?;
        index_rows.push(row);
    } else if v.is_boolean() {
        let mut value = vec![INDEX_TYPE_NUMBER];
        if v.as_bool().unwrap() {
            //value.push(1);
            value.extend_from_slice(&1_f64.to_be_bytes());
        } else {
            //value.push(0);
            value.extend_from_slice(&0_f64.to_be_bytes());
        };
        let row = create_row(viewname, &key, &value, is_old_idx, rkey, viewentry)?;
        index_rows.push(row);
    } else if v.is_number() {
        let val_num = v.as_f64().unwrap();
        let value = [&[INDEX_TYPE_NUMBER], &val_num.to_be_bytes()[..]].concat();
        let row = create_row(viewname, &key, &value, is_old_idx, rkey, viewentry)?;
        index_rows.push(row);
    } else if v.is_string() {
        let str_val = v.as_str().unwrap();
        if str_val.contains(char::from(0)) {
            return Err(AppError::new(
                500,
                format!(
                    "String value of property {}_{} contains NULL characters",
                    viewname.to_string(),
                    key
                )
                .as_str(),
            ));
        }
        let value = [&[INDEX_TYPE_STRING], str_val.as_bytes()].concat();
        let row = create_row(viewname, &key, &value, is_old_idx, rkey, viewentry)?;
        index_rows.push(row);
    } else if v.is_object() {
        let value = [&[INDEX_TYPE_STRING], &serde_json::to_vec(v).unwrap()[..]].concat();
        let row = create_row(viewname, &key, &value, is_old_idx, rkey, viewentry)?;
        index_rows.push(row);
    } else if v.is_array() {
        for av in v.as_array().unwrap() {
            // recursive array insert
            let rows = create_rows(viewname, &key, &av, is_old_idx, rkey, viewentry.clone())?;
            index_rows.extend(rows);
        }
    }
    //log::debug!("index_rows: {:?}", index_rows);
    Ok(index_rows)
}

fn create_row(
    viewname: &str,
    key: &str,
    value: &[u8],
    is_old_idx: bool,
    rkey: &ResourceKey,
    viewentry: Arc<RwLock<ViewEntry>>,
) -> Result<IndexRecord, AppError> {
    // TODO: set size constraint in database
    if value.len() > 1501 {
        return Err(AppError::new(
            500,
            format!(
                "The value of property {}_{} ({} bytes) must not be longer than 1500 bytes",
                viewname.to_string(),
                key,
                value.len()
            )
            .as_str(),
        ));
    }
    Ok(IndexRecord::new(
        rkey.dir.clone(),
        viewname.to_string(),
        key.to_string(),
        value.to_vec(),
        rkey.name.clone(),
        viewentry,
        is_old_idx,
    ))
}
