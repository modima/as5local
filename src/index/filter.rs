use crate::adapter::db::IndexEntry;
use crate::db::{filter::OP, query::StoreQuery};
use crate::http::errors::AppError;
use std::{collections::HashMap, str};

const MIN_CHAR: &str = " ";
const MAX_CHAR: &str = "\u{FFFF}";

#[derive(Debug)]
pub struct IndexFilter {
    pub cursor: Vec<u8>,
    pub nameonly: bool,
    pub order_by: (String, String),
    pub ranges: HashMap<String, (Vec<u8>, Vec<u8>)>,
}

impl IndexFilter {
    pub fn new(sq: &StoreQuery) -> Result<IndexFilter, AppError> {
        let mut filter = IndexFilter {
            cursor: sq.get_cursor()?,
            nameonly: sq.is_nameonly(),
            order_by: ("".to_string(), "asc".to_string()),
            ranges: HashMap::new(),
        };
        //let mut inequality_field = String::new();
        for p in sq.parameters.values().cloned() {
            let mut rangestart: Vec<u8> = Vec::new();
            let mut rangeend: Vec<u8> = Vec::new();
            if !p.is_reserved() {
                log::debug!("param: {:?}", p);
                let name = p.attribute_name.clone();
                let values = p.as_index_values()?;
                if filter.order_by.0.len() == 0 {
                    filter.order_by.0 = name.to_string();
                }
                /*
                if inequality_field.len() > 0 && !inequality_field.eq(&name) {
                    return Err(AppError::new(
                        500,
                        &format!(
                            "Using multiple inequality filters is only allowed on a single property: {}",
                            inequality_field
                        ),
                    ));
                }
                */
                /*
                if p.operator != OP::EQ {
                    inequality_field = name.clone();
                }
                */
                for value in values {
                    log::debug!("value: {:?}", value);
                    match p.operator {
                        OP::EQ => {
                            /*
                            if inequality_field.len() > 0 && filter.nameonly {
                                return Err(AppError::new(
                                    500,
                                    &format!(
                                        "Cannot use projection on a property with an equality filter: {}",
                                        name
                                    ),
                                ));
                            }
                            */
                            //if rangestart.cmp(&value).is_lt() {
                            rangestart = value;
                            if p.flags > 0 {
                                let mut v_clone = rangestart.clone();
                                *v_clone.last_mut().unwrap() += 1;
                                rangeend = v_clone
                            } else {
                                rangeend = [&rangestart, MIN_CHAR.as_bytes()].concat()
                            };
                            //}
                        }
                        OP::LT => {
                            //if rangeend.cmp(&value).is_gt() {
                            rangeend = value;
                            //}
                        }
                        OP::LE => {
                            //if rangeend.cmp(&value).is_gt() {
                            if p.flags > 0 {
                                let mut v_clone = value;
                                *v_clone.last_mut().unwrap() += 1;
                                rangeend = v_clone
                            } else {
                                rangeend = [&value, MIN_CHAR.as_bytes()].concat()
                            };
                            //}
                        }
                        OP::GT => {
                            //if rangestart.cmp(&value).is_lt() {
                            if p.flags > 0 {
                                let mut v_clone = value;
                                *v_clone.last_mut().unwrap() += 1;
                                rangestart = v_clone
                            } else {
                                rangestart = [&value, MIN_CHAR.as_bytes()].concat()
                            };
                            //}
                        }
                        OP::GE => {
                            //if rangestart.cmp(&value).is_lt() {
                            log::debug!("OP::GE --> {:?}", value);
                            rangestart = value;
                            rangeend = [&rangestart[0..1], MAX_CHAR.as_bytes()].concat();
                            //}
                        }
                        OP::SW => {
                            if rangestart.is_empty() {
                                log::debug!("OP::SW --> {:?}", value);
                                rangestart = value.clone();
                                if p.flags > 0 {
                                    rangeend = value;
                                } else {
                                    rangeend = [&value, MAX_CHAR.as_bytes()].concat();
                                }
                            }
                        }
                        OP::OB => {
                            filter.order_by = (name.clone(), String::from_utf8(value).unwrap());
                            if filter.ranges.is_empty() {
                                filter.ranges.insert(
                                    name.clone(),
                                    ("".as_bytes().to_vec(), "".as_bytes().to_vec()),
                                );
                            } else if !filter.ranges.contains_key(&name) {
                                return Err(AppError::new(
                                    500,
                                    &format!(
                                        "Cannot order by property '{}' without a corresponding filter expression",
                                        name
                                    ),
                                ));
                            }
                        }
                    }
                    if filter.nameonly && filter.order_by.0.len() == 0 {
                        filter.order_by.0 = name.clone()
                    }
                    log::debug!(
                        "check rangestart {:?} --> {:?}",
                        filter
                            .ranges
                            .get(&name)
                            .unwrap_or(&("".as_bytes().to_vec(), "".as_bytes().to_vec()))
                            .0,
                        rangestart
                    );
                    if rangestart.len() > 0
                        && (!filter.ranges.contains_key(&name)
                            || filter.ranges.get(&name).unwrap().0.len() == 0
                            || rangestart.cmp(&filter.ranges.get(&name).unwrap().0).is_gt())
                    {
                        log::debug!("set rangestart to {:?}", rangestart);
                        filter
                            .ranges
                            .entry(name.clone())
                            .or_insert(("".as_bytes().to_vec(), "".as_bytes().to_vec()))
                            .0 = rangestart.to_vec();
                    }
                    log::debug!(
                        "check rangeend {:?} --> {:?}",
                        filter
                            .ranges
                            .get(&name)
                            .unwrap_or(&("".as_bytes().to_vec(), "".as_bytes().to_vec()))
                            .1,
                        rangeend
                    );
                    if rangeend.len() > 0
                        && (!filter.ranges.contains_key(&name)
                            || filter.ranges.get(&name).unwrap().1.len() == 0
                            || rangeend.cmp(&filter.ranges.get(&name).unwrap().1).is_lt()
                        )
                    {
                        log::debug!("set rangeend to {:?}", rangeend);
                        filter
                            .ranges
                            .entry(name.clone())
                            .or_insert(("".as_bytes().to_vec(), "".as_bytes().to_vec()))
                            .1 = rangeend.to_vec();
                    }
                }
            }
        }
        if filter.ranges.is_empty() {
            // If no filter defined --> query dummy row
            filter.ranges.insert(
                "_dummy_".to_string(),
                ("".as_bytes().to_vec(), "".as_bytes().to_vec()),
            );
        }
        Ok(filter)
    }

    pub fn get_type_prefix(&self, key: &str) -> &[u8] {
        let range_start = self.get_rangestart(key);
        if !range_start.is_empty() {
            return &range_start[0..1];
        } else {
            return range_start;
        }
    }

    pub fn get_rangestart(&self, key: &str) -> &[u8] {
        match self.ranges.get(key) {
            Some((rangestart, rangeend)) => {
                if rangestart.len() > 0 {
                    rangestart
                } else if rangeend.len() > 0 {
                    &rangeend[0..1]
                } else {
                    &[]
                }
            }
            None => &[],
        }
    }
    pub fn get_rangeend(&self, key: &str) -> &[u8] {
        match self.ranges.get(key) {
            Some((rangestart, rangeend)) => {
                if rangeend.len() > 0 {
                    rangeend
                } else if rangestart.len() > 0 {
                    &rangestart[0..1]
                } else {
                    &[]
                }
            }
            None => &[],
        }
    }

    pub fn matches(&self, ie: &IndexEntry) -> bool {
        if !self.ranges.is_empty() {
            let key = self.ranges.keys().next().unwrap();
            if !ie.fieldname.cmp(&key).is_eq() {
                log::debug!("fieldname: {} != key: {}", ie.fieldname, key);
                return false;
            }
            let (rangestart, rangeend) = self.ranges.get(key).unwrap();
            if rangestart.len() > 0
                && (ie.fieldvalue.cmp(rangestart).is_lt() || ie.fieldvalue[0] != rangestart[0])
            {
                log::debug!("key: {:?} < rangestart: {:?}", ie.fieldvalue, rangestart,);
                return false;
            }
            if rangeend.len() > 0
                && (ie.fieldvalue.cmp(rangeend).is_ge() || ie.fieldvalue[0] != rangeend[0])
            {
                log::debug!("key: {:?} >= rangeend: {:?}", ie.fieldvalue, rangeend);
                return false;
            }
        }
        return true;
    }
}
