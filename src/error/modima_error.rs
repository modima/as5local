use std::error::Error;
use std::fmt::{Debug, Display, Formatter};
use std::panic::Location;

pub struct ModimaError {
    location: String,
    error_string: String,
}

impl ModimaError {
    pub fn new_from_error(location: &Location, error: Box<dyn Debug>)->ModimaError {
        ModimaError::new_from_string(location, format!("{:?}", error))
    }
    pub fn new_from_string (location: &Location, error_string: String)->ModimaError {
        ModimaError {
            location: location.to_string(),
            error_string: error_string.replace("\n", "\n\t\t")
        }
    }
}
impl Debug for ModimaError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "\n\tsource: \n\t\t{} \n\terror:    \n\t\t{}", self.location,self.error_string)
    }
}

impl Display for ModimaError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "\n\tsource: \n\t\t{} \n\terror:    \n\t\t{}", self.location,self.error_string)
    }
}

impl Error for ModimaError {}
