use crate::adapter::db::{Cursor, FileRow, IndexEntry, DB};
use crate::db::filter::NameFilter;
use crate::http::{
    errors::AppError,
    resource::{Resource, ResourceKey},
};
use crate::index::{
    filter::IndexFilter,
    record::IndexRecord,
    results::{IndexResult, IndexResultSet},
};
use crate::utils;
use async_trait::async_trait;
use bytes::Bytes;
use dotenv;
use maplit::hashmap;
use md5::{Digest, Md5};
use rocksdb::{
    backup::{BackupEngine, BackupEngineOptions},
    DBWithThreadMode, Env, Options, SingleThreaded, WriteBatch,
};
use std::{convert::TryInto, str, time::Instant};
use tokio_cron_scheduler::{Job, JobScheduler};

const INDEX_DELIMITER: &[u8] = &[b'\0'];
const FILE_DELIMITER: &[u8] = &[b'~'];
const PFX_OBJ: &[u8] = "o:".as_bytes();
const PFX_IDX: &[u8] = "i:".as_bytes();
const PFX_META: &[u8] = "m:".as_bytes();
const SFX_VERSION: &[u8] = "0_version".as_bytes();
const SFX_VEIDS: &[u8] = "1_veids".as_bytes();
const SFX_VIEWS: &[u8] = "2_views".as_bytes();
const SFX_DATA: &[u8] = "3_data".as_bytes();
const SFX_ENCODING: &[u8] = "4_encoding".as_bytes();
const SFX_MODIFIED: &[u8] = "5_modified".as_bytes();

pub struct RocksDB {
    pool: DBWithThreadMode<SingleThreaded>,
}

impl RocksDB {
    fn extract_name(slice: &[u8]) -> &[u8] {
        match slice.iter().rposition(|&x| x == b'/') {
            Some(p1) => match slice.iter().skip(p1 + 1).position(|&y| y == b'\0') {
                Some(p2) => &slice[p1 + 1..p1 + p2 + 1],
                None => &slice[p1 + 1..],
            },
            None => slice,
        }
    }
    fn extract_dir(slice: &[u8], offset: usize) -> &[u8] {
        match slice.iter().skip(offset).position(|&x| x == b'/') {
            Some(p) => &slice[offset..offset + p],
            None => &[],
        }
    }
    fn split_index(slice: &[u8]) -> IndexEntry {
        let mut splits = Vec::new();
        if let Some(p1) = slice.iter().position(|&x| x == b'~') {
            let mut index = &slice[p1 + 1..];
            loop {
                match index.iter().position(|&y| y == b'\0') {
                    Some(p2) => {
                        let val = &index[..p2];
                        // 8 byte float 64 value (can contain zeros)?
                        if splits.len() == 2 && val.len() > 0 && val[0] == b'n' {
                            splits.push(&index[..p2 + 9 - val.len()]);
                            index = &index[p2 + 10 - val.len()..];
                        } else {
                            splits.push(val);
                            index = &index[p2 + 1..];
                        }
                    }
                    None => {
                        splits.push(index);
                        let mut f: Vec<&[u8]> = vec!["".as_bytes(); 5 - splits.len()];
                        splits.append(&mut f);
                        break;
                    }
                }
            }
        }
        IndexEntry {
            viewname: str::from_utf8(splits.get(0).unwrap()).unwrap().to_string(),
            fieldname: str::from_utf8(splits.get(1).unwrap()).unwrap().to_string(),
            fieldvalue: splits.get(2).unwrap(),
            name: str::from_utf8(splits.get(3).unwrap()).unwrap().to_string(),
            viewentry_id: str::from_utf8(splits.get(4).unwrap()).unwrap().to_string(),
        }
    }
}

#[async_trait]
impl DB for RocksDB {
    fn create() -> Box<dyn DB> {
        let storage_path = &dotenv::var("ROCKSDB_DATAPATH").unwrap();
        let mut opts = Options::default();

        opts.increase_parallelism(12);
        opts.optimize_level_style_compaction(536870912);
        // opts.set_max_write_buffer_number(512);
        opts.set_write_buffer_size(536870912);
        // opts.set_target_file_size_base(1073741824);

        opts.create_if_missing(true);
        let pool = rocksdb::DB::open(&opts, &storage_path).unwrap();
        Box::new(RocksDB { pool: pool })
    }

    async fn init(&self) -> Result<(), AppError> {
        Ok(())
    }

    async fn insert<'a>(&self, r: &'a mut Resource) -> Result<bool, AppError> {
        let mut batch = WriteBatch::default();

        // store metadata
        batch.put(
            &[
                PFX_META,
                r.path.as_bytes(),
                FILE_DELIMITER,
                r.name.as_bytes(),
            ]
            .concat(),
            [
                &r.version.to_be_bytes() as &[u8],
                &[b'\0'],
                r.modified.as_bytes(),
                &[b'\0'],
                r.encoding.as_ref().unwrap_or(&"".to_string()).as_bytes(),
            ]
            .concat(),
        );

        // store data
        let pfx = &[PFX_OBJ, r.path_as_hash().as_bytes(), FILE_DELIMITER].concat();
        batch.put(&[pfx, SFX_VERSION].concat(), r.version.to_be_bytes());
        if r.viewentry_ids.len() > 0 {
            batch.put(
                &[pfx, SFX_VEIDS].concat(),
                utils::vec_string_as_bytes(r.viewentry_ids.iter().map(|s| s as &str).collect()),
            );
        }
        if r.views.len() > 0 {
            batch.put(&[pfx, SFX_VIEWS].concat(), r.views_as_gzip()?);
        }
        batch.put(&[pfx, SFX_DATA].concat(), r.data_as_gzip()?);
        if r.encoding.is_some() {
            batch.put(
                &[pfx, SFX_ENCODING].concat(),
                r.encoding.as_ref().unwrap_or(&"".to_string()).as_bytes(),
            );
        }
        batch.put(&[pfx, SFX_MODIFIED].concat(), &r.modified);
        match self.pool.write(batch) {
            Ok(()) => Ok(true),
            Err(e) => Err(e.into()),
        }
    }

    async fn update<'a>(&self, r: &'a mut Resource, old_version: i32) -> Result<bool, AppError> {
        let key = &[
            PFX_OBJ,
            r.path_as_hash().as_bytes(),
            FILE_DELIMITER,
            SFX_VERSION,
        ]
        .concat();
        let val = self.pool.get(key)?;
        if val.is_some() {
            let db_version = i32::from_be_bytes(val.unwrap().try_into().unwrap());
            if db_version != old_version {
                return Ok(false);
            }
        }
        self.insert(r).await
    }

    async fn get(&self, rkey: &ResourceKey) -> Result<Option<Resource>, AppError> {
        let pfx = &[PFX_OBJ, rkey.path_as_hash().as_bytes(), FILE_DELIMITER].concat();

        log::debug!(
            "GET rkey: {:?} | pfx: {}",
            rkey,
            str::from_utf8(pfx).unwrap()
        );

        let snapshot = self.pool.snapshot();
        let version = snapshot
            .get(&[pfx, SFX_VERSION].concat())?
            .unwrap_or(vec![]);

        if version.len() == 0 {
            return Ok(None);
        }

        let viewentry_ids = snapshot.get(&[pfx, SFX_VEIDS].concat())?.unwrap_or(vec![]);
        let views = snapshot.get(&[pfx, SFX_VIEWS].concat())?.unwrap_or(vec![]);
        let data = snapshot.get(&[pfx, SFX_DATA].concat())?.unwrap_or(vec![]);
        let encoding = snapshot
            .get(&[pfx, SFX_ENCODING].concat())?
            .unwrap_or(vec![]);
        let modified = snapshot
            .get(&[pfx, SFX_MODIFIED].concat())?
            .unwrap_or(vec![]);

        Ok(Some(Resource::new(
            rkey.dir.clone(),
            rkey.name.clone(),
            Bytes::from(data),
            Some(Bytes::from(views)),
            i32::from_be_bytes(version.try_into().unwrap()),
            str::from_utf8(&modified)?.to_string(),
            utils::bytes_as_vec_string(viewentry_ids)?,
            if encoding.len() > 0 {
                Some(str::from_utf8(&encoding)?.to_string())
            } else {
                None
            },
            true,
        )?))
    }

    async fn delete(&self, rkey: &ResourceKey) -> Result<bool, AppError> {
        let pfx_obj = &[PFX_OBJ, rkey.path_as_hash().as_bytes(), FILE_DELIMITER].concat();
        let path_meta = &[
            PFX_META,
            rkey.dir.as_bytes(),
            FILE_DELIMITER,
            rkey.name.as_bytes(),
        ]
        .concat();
        log::debug!("delete: {}", str::from_utf8(path_meta).unwrap());
        let mut batch = WriteBatch::default();
        batch.delete(path_meta);
        batch.delete(&[pfx_obj, SFX_VERSION].concat());
        batch.delete(&[pfx_obj, SFX_VEIDS].concat());
        batch.delete(&[pfx_obj, SFX_VIEWS].concat());
        batch.delete(&[pfx_obj, SFX_DATA].concat());
        batch.delete(&[pfx_obj, SFX_ENCODING].concat());
        batch.delete(&[pfx_obj, SFX_MODIFIED].concat());
        match self.pool.write(batch) {
            Ok(()) => Ok(true),
            Err(e) => Err(e.into()),
        }
    }

    async fn list_files(
        &self,
        path: &str,
        filter: &NameFilter,
        limit: i32,
    ) -> Result<Vec<FileRow>, AppError> {
        log::debug!(
            "list_files path: {} filter: {:?} limit: {}",
            path,
            filter,
            limit,
        );

        let mut results = Vec::new();
        let key_prfx = &[PFX_META, path.as_bytes(), FILE_DELIMITER].concat();
        let mut iter = self.pool.raw_iterator();
        let start_key: Vec<u8>;
        if filter.desc_order {
            start_key = [key_prfx, filter.get_rangeend()].concat();
            iter.seek(PFX_OBJ);
            iter.seek_for_prev(&start_key);
        } else {
            start_key = [key_prfx, filter.rangestart.as_bytes()].concat();
            iter.seek(&start_key);
        };
        log::debug!(
            "list_files range start: {}",
            str::from_utf8(&start_key).unwrap()
        );
        while iter.valid() && results.len() < limit as usize {
            match iter.key() {
                Some(key) => {
                    // check if key matches prefix
                    for (a, b) in key_prfx.iter().zip(key.iter()) {
                        if a != b {
                            return Ok(results);
                        }
                    }
                    let name = RocksDB::extract_name(&key);
                    log::debug!(
                        "key: {} | name: {}",
                        str::from_utf8(&key).unwrap(),
                        str::from_utf8(&name).unwrap()
                    );
                    results.push(FileRow {
                        name: str::from_utf8(&name[1..]).unwrap().to_string(), // remove preceding ~ (file indicator)
                    });
                    if filter.desc_order {
                        iter.prev()
                    } else {
                        iter.next()
                    }
                }
                None => {
                    log::debug!("no match");
                    break;
                }
            }
        }
        Ok(results)
    }

    async fn list_dirs(
        &self,
        path: &str,
        filter: &NameFilter,
        limit: i32,
    ) -> Result<Vec<FileRow>, AppError> {
        log::debug!(
            "list_dirs path: {} filter: {:?} limit: {}",
            path,
            filter,
            limit,
        );
        let mut results = Vec::new();
        let key_prfx = &[PFX_META, path.as_bytes()].concat();
        let mut iter = self.pool.raw_iterator();
        let start_key: Vec<u8>;
        if filter.desc_order {
            start_key = [key_prfx, filter.get_rangeend()].concat();
            iter.seek(PFX_OBJ);
            iter.seek_for_prev(&start_key);
        } else {
            start_key = [key_prfx, filter.rangestart.as_bytes()].concat();
            iter.seek(&start_key);
        };
        log::debug!(
            "list_dirs range start: {}",
            str::from_utf8(&start_key).unwrap()
        );
        while iter.valid() && results.len() < limit as usize {
            match iter.key() {
                Some(key) => {
                    // check if key matches prefix
                    for (a, b) in key_prfx.iter().zip(key.iter()) {
                        if a != b {
                            return Ok(results);
                        }
                    }
                    let name = RocksDB::extract_dir(&key, key_prfx.len());
                    log::debug!(
                        "key: {} | name: {}",
                        str::from_utf8(&key).unwrap(),
                        str::from_utf8(&name).unwrap(),
                    );
                    if name.len() == 0 {
                        // start of files
                        break;
                    }
                    results.push(FileRow {
                        name: str::from_utf8(name).unwrap().to_string(),
                    });
                    if filter.desc_order {
                        // decrement last char (must be ASCII 7)
                        let mut cursor = name.to_vec();
                        *cursor.last_mut().unwrap() -= 1;
                        iter.seek_for_prev(&[key_prfx, &cursor[..]].concat())
                    } else {
                        // append next ASCII 7 char after '/' --> '0'
                        let cursor = &[name, &[b'0']].concat();
                        iter.seek(&[key_prfx, &cursor[..]].concat())
                    }
                }
                None => {
                    log::debug!("no match");
                    break;
                }
            }
        }
        Ok(results)
    }

    async fn list_views(
        &self,
        path: &str,
        filter: &NameFilter,
        limit: i32,
    ) -> Result<Vec<FileRow>, AppError> {
        log::debug!(
            "list_views path: {} filter: {:?} limit: {}",
            path,
            filter,
            limit,
        );
        let mut results = Vec::new();
        let key_prfx = &[PFX_IDX, path.as_bytes(), FILE_DELIMITER].concat();
        let mut iter = self.pool.raw_iterator();
        let start_key: Vec<u8>;
        if filter.desc_order {
            start_key = [key_prfx, filter.get_rangeend()].concat();
            iter.seek(PFX_META);
            iter.seek_for_prev(&start_key);
        } else {
            start_key = [key_prfx, filter.rangestart.as_bytes()].concat();
            iter.seek(&start_key);
        };
        log::debug!(
            "list_views range start: {}",
            str::from_utf8(&start_key).unwrap()
        );
        while iter.valid() && results.len() < limit as usize {
            match iter.key() {
                Some(key) => {
                    // check if key matches prefix
                    for (a, b) in key_prfx.iter().zip(key.iter()) {
                        if a != b {
                            return Ok(results);
                        }
                    }
                    let name = RocksDB::extract_name(&key);
                    log::debug!(
                        "key: {} | name: {}",
                        str::from_utf8(&key).unwrap(),
                        str::from_utf8(&name).unwrap(),
                    );
                    results.push(FileRow {
                        name: str::from_utf8(&name[1..]).unwrap().to_string(), // remove preceding ~ (view indicator)
                    });
                    if filter.desc_order {
                        // decrement last char (must be ASCII 7)
                        let mut cursor = name[1..].to_vec();
                        *cursor.last_mut().unwrap() -= 1;
                        iter.seek_for_prev(&[key_prfx, &cursor[..]].concat())
                    } else {
                        // append first ASCII 7 char (Space)
                        let cursor = &[&name[1..], &[b' ']].concat();
                        iter.seek(&[key_prfx, &cursor[..]].concat())
                    }
                }
                None => {
                    log::debug!("no match");
                    break;
                }
            }
        }
        Ok(results)
    }

    async fn insert_index_rows(&self, rows: Vec<&IndexRecord>) -> Result<(), AppError> {
        log::debug!("insert_index_rows: {}", rows.len());
        if rows.len() == 0 {
            return Ok(());
        }
        let mut batch = WriteBatch::default();
        rows.into_iter().for_each(|row| {
            let key = &[PFX_IDX, &row.key_as_bytes()].concat();
            log::debug!("insert_row: {:?}", str::from_utf8(key));
            batch.put(key, "");
        });
        match self.pool.write(batch) {
            Ok(()) => Ok(()),
            Err(e) => Err(e.into()),
        }
    }
    async fn delete_index_rows(&self, rows: Vec<&IndexRecord>) -> Result<(), AppError> {
        log::debug!("delete_index_rows: {}", rows.len());
        if rows.len() == 0 {
            return Ok(());
        }
        let mut batch = WriteBatch::default();
        rows.into_iter().for_each(|row| {
            batch.delete(&[PFX_IDX, &row.key_as_bytes()].concat());
        });
        match self.pool.write(batch) {
            Ok(()) => Ok(()),
            Err(e) => Err(e.into()),
        }
    }

    async fn filter_index(
        &self,
        path: &str,
        viewname: &str,
        namefilter: &NameFilter,
        indexfilter: &IndexFilter,
        limit: i32,
    ) -> Result<IndexResultSet, AppError> {
        log::debug!("filter_index path: {} limit: {}", path, limit);
        let mut key_prfx = [
            PFX_IDX,
            path.as_bytes(),
            FILE_DELIMITER,
            viewname.as_bytes(),
            INDEX_DELIMITER,
        ]
        .concat()
        .to_vec();
        let mut filter_key = "";
        if !indexfilter.ranges.is_empty() {
            filter_key = indexfilter.ranges.keys().next().unwrap();
            key_prfx.extend(filter_key.as_bytes());
            key_prfx.extend(INDEX_DELIMITER);
        }
        if indexfilter.cursor.len() > 0 {
            let cpath;
            match indexfilter.cursor.iter().position(|&y| y == b'\0') {
                Some(p) => {
                    cpath = str::from_utf8(&indexfilter.cursor[..p]).unwrap();
                    let clen = indexfilter.cursor.len();
                    key_prfx.extend(&indexfilter.cursor[p + 1..clen - 2]); // remove delimiter '\0' and row_idx from cursor
                                                                           //log::debug!("prefix 1: {:?}", str::from_utf8(&key_prfx).unwrap());
                }
                None => {
                    return Err(AppError::new(
                        500,
                        &format!(
                            "Invalid cursor {}",
                            str::from_utf8(&indexfilter.cursor).unwrap()
                        ),
                    ))
                }
            };
            if !cpath.eq(path) {
                return Err(AppError::new(
                    500,
                    &format!(
                        "Invalid cursor {}",
                        str::from_utf8(&indexfilter.cursor).unwrap()
                    ),
                ));
            }
        } else if indexfilter.order_by.1.eq("desc") {
            key_prfx.extend(indexfilter.get_rangeend(filter_key));
        } else {
            key_prfx.extend(indexfilter.get_rangestart(filter_key));
        }
        //log::debug!("prefix 2: {:?}", str::from_utf8(&key_prfx).unwrap());
        let mut iter = self.pool.raw_iterator();
        if indexfilter.order_by.1.eq("desc") {
            iter.seek(PFX_META);
            iter.seek_for_prev(&key_prfx);
            log::debug!(
                "search backwards using startkey: {:?}",
                str::from_utf8(&key_prfx).unwrap()
            );
        } else {
            iter.seek(&key_prfx);
            log::debug!(
                "search forwards using startkey: {:?}",
                str::from_utf8(&key_prfx).unwrap()
            );
        };
        let mut results: Vec<IndexResult> = Vec::new();
        let mut next_cursor = Vec::new();
        //let mut count = 0;
        let mut row_idx = 0;
        while iter.valid() {
            match iter.key() {
                Some(key) => {
                    log::debug!("key matched: {:?}", str::from_utf8(&key));
                    let ie = RocksDB::split_index(&key);
                    if !indexfilter.matches(&ie) {
                        break;
                    }
                    if (results.is_empty()
                        && indexfilter.cursor.len() > 0
                        && key.starts_with(&key_prfx))
                        || !namefilter.matches(&ie.name)
                    {
                        if indexfilter.order_by.1.eq("desc") {
                            iter.prev();
                        } else {
                            iter.next();
                        }
                        continue;
                    }
                    //let ve_key = format!("{}{}", ie.name, ie.viewentry_id);
                    let mut views = None;
                    let mut veids = None;
                    // load views and viewentry ids (if not nameonly)
                    if !indexfilter.nameonly {
                        let hash = format!(
                            "{:x}",
                            Md5::digest(&[path.as_bytes(), ie.name.as_bytes()].concat())
                        )
                        .to_string();
                        let pfx = &[PFX_OBJ, hash.as_bytes(), FILE_DELIMITER].concat();
                        log::debug!(
                            "load views: {}",
                            str::from_utf8(&[pfx, SFX_VIEWS].concat()).unwrap()
                        );
                        let snapshot = self.pool.snapshot();
                        views = snapshot.get(&[pfx, SFX_VIEWS].concat())?;
                        let veids_opt = snapshot.get(&[pfx, SFX_VEIDS].concat())?;
                        if veids_opt.is_some() {
                            veids = Some(utils::bytes_as_vec_string(veids_opt.unwrap())?)
                        }
                    }
                    let ir = IndexResult {
                        name: ie.name,
                        viewentry_id: ie.viewentry_id,
                        columns: hashmap! {
                            ie.fieldname.clone() => vec![ie.fieldvalue.to_vec()],
                        },
                        views: views,
                        viewentry_ids: veids,
                    };
                    if let Some(lr) = results.last() {
                        if ir.eq(lr) {
                            row_idx += 1;
                            log::debug!("{} equal results round", row_idx + 1);
                        } else {
                            row_idx = 0;
                        }
                    }
                    if !indexfilter.ranges.is_empty()
                        && indexfilter.ranges.keys().next().unwrap().eq(&ie.fieldname)
                    {
                        let name = ir.name.clone();
                        let veid = ir.viewentry_id.clone();
                        results.push(ir);
                        //count += 1;
                        if results.len() == limit as usize {
                            next_cursor = Cursor {
                                path: path.to_string(),
                                name: name,
                                viewentry_id: veid,
                                fieldvalue: ie.fieldvalue,
                                row_idx: row_idx,
                            }
                            .to_bytes();
                            break;
                        }
                    }
                    if indexfilter.order_by.1.eq("desc") {
                        iter.prev();
                    } else {
                        iter.next();
                    }
                }
                None => {
                    log::debug!("no match");
                    break;
                }
            }
        }
        Ok(IndexResultSet::new(results, &next_cursor))
    }

    async fn schedule_backup(&'static self) {
        let bp = &dotenv::var("ROCKSDB_BACKUP_PATH").unwrap_or("".to_string());
        let backup_path = bp.to_string();
        let bs = &dotenv::var("ROCKSDB_BACKUP_SCHEDULE_UTC").unwrap_or("".to_string());
        let backup_schedule = bs.to_string();
        log::info!(
            "schedule backup at {:?} | storage path: {:?}",
            backup_schedule,
            backup_path,
        );
        if backup_path.len() > 0 && backup_schedule.len() > 0 {
            let backup_job = Job::new(backup_schedule.as_str(), move |_uuid, _l| {
                log::info!("rocksdb backup started");
                let backup_started = Instant::now();
                let env = Env::new().unwrap();
                let backup_opts = BackupEngineOptions::new(backup_path.as_str()).unwrap();
                let mut backup_engine = BackupEngine::open(&backup_opts, &env).unwrap();
                match backup_engine.create_new_backup_flush(&self.pool, true) {
                    Ok(_) => {
                        let duration = backup_started.elapsed();
                        log::info!("rocksdb backup completed after {:?}", duration)
                    }
                    Err(e) => log::error!("rocksdb backup failed: {}", e),
                }
            })
            .unwrap();
            let scheduler = JobScheduler::new().await.unwrap();
            match scheduler.add(backup_job).await {
                Ok(_) => log::info!("backup job added"),
                Err(e) => log::error!("add backup job failed: {}", e),
            }
            match scheduler.start().await {
                Ok(_) => log::info!("backup scheduler started"),
                Err(e) => log::error!("start scheduler failed: {}", e),
            }
        }
    }
}
