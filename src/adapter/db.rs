use crate::db::filter::NameFilter;
use crate::http::{
    errors::AppError,
    resource::{Resource, ResourceKey},
};
use crate::index::{filter::IndexFilter, record::IndexRecord, results::IndexResultSet};
use async_trait::async_trait;
use std::str;

#[derive(Debug)]
pub struct Cursor<'a> {
    pub path: String,
    pub name: String,
    pub viewentry_id: String,
    pub fieldvalue: &'a [u8],
    pub row_idx: u8,
}

impl Cursor<'_> {
    pub fn to_bytes(&self) -> Vec<u8> {
        /*
        log::debug!("PATH: {:?}", self.path.as_bytes());
        log::debug!("FIELDVALUE: {:?}", self.fieldvalue);
        log::debug!("NAME: {:?}", self.name.as_bytes());
        log::debug!("VIEWENTRY_ID: {:?}", self.viewentry_id.as_bytes());
        */
        [
            self.path.as_bytes(),
            &[b'\0'],
            self.fieldvalue,
            &[b'\0'],
            self.name.as_bytes(),
            &[b'\0'],
            self.viewentry_id.as_bytes(),
            &[b'\0'],
            &[self.row_idx],
        ]
        .concat()
    }

    pub fn from_bytes(mut slice: &[u8]) -> Option<Cursor> {
        if slice.len() == 0 {
            return None;
        }
        log::debug!("cursor: {:?}", slice);
        let mut splits = Vec::new();
        loop {
            match slice.iter().position(|&y| y == b'\0') {
                Some(p2) => {
                    let val = &slice[..p2];
                    // 8 byte float 64 value (can contain zeros)?
                    if splits.len() == 1 && val.len() > 0 && val[0] == b'n' {
                        splits.push(&slice[..p2 + 9 - val.len()]);
                        slice = &slice[p2 + 10 - val.len()..];
                    } else {
                        splits.push(val);
                        slice = &slice[p2 + 1..];
                    }
                }
                None => {
                    splits.push(slice);
                    break;
                }
            }
            log::debug!("rest: {:?}", slice);
            // rest is row_idx (can be 0)
            if splits.len() == 4 {
                splits.push(slice);
                break;
            }
        }
        log::debug!("SPLITS: {:?}", splits);
        Some(Cursor {
            path: str::from_utf8(splits.get(0).unwrap()).unwrap().to_string(),
            fieldvalue: splits.get(1).unwrap(),
            name: str::from_utf8(splits.get(2).unwrap()).unwrap().to_string(),
            viewentry_id: str::from_utf8(splits.get(3).unwrap()).unwrap().to_string(),
            row_idx: splits.get(4).unwrap()[0],
        })
    }
}

#[derive(Debug, sqlx::FromRow)]
pub struct FileRow {
    pub name: String,
}

#[derive(Debug)]
pub struct IndexEntry<'a> {
    pub viewname: String,
    pub fieldname: String,
    pub fieldvalue: &'a [u8],
    pub name: String,
    pub viewentry_id: String,
}

#[async_trait]
pub trait DB: Send + Sync {
    fn create() -> Box<dyn DB>
    where
        Self: Sized;
    async fn init(&self) -> Result<(), AppError>;
    async fn insert<'a>(&self, r: &'a mut Resource) -> Result<bool, AppError>;
    async fn update<'a>(&self, r: &'a mut Resource, old_version: i32) -> Result<bool, AppError>;
    async fn get(&self, rkey: &ResourceKey) -> Result<Option<Resource>, AppError>;
    async fn delete(&self, rkey: &ResourceKey) -> Result<bool, AppError>;
    async fn list_files(
        &self,
        path: &str,
        filter: &NameFilter,
        limit: i32,
    ) -> Result<Vec<FileRow>, AppError>;
    async fn list_dirs(
        &self,
        path: &str,
        filter: &NameFilter,
        limit: i32,
    ) -> Result<Vec<FileRow>, AppError>;
    async fn list_views(
        &self,
        path: &str,
        filter: &NameFilter,
        limit: i32,
    ) -> Result<Vec<FileRow>, AppError>;
    async fn insert_index_rows(&self, rows: Vec<&IndexRecord>) -> Result<(), AppError>;
    async fn delete_index_rows(&self, rows: Vec<&IndexRecord>) -> Result<(), AppError>;
    async fn filter_index(
        &self,
        path: &str,
        viewname: &str,
        namefilter: &NameFilter,
        indexfilter: &IndexFilter,
        limit: i32,
    ) -> Result<IndexResultSet, AppError>;
    async fn schedule_backup(&'static self);
}
