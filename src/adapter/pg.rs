use crate::adapter::db::{Cursor, FileRow, IndexEntry, DB};
use crate::db::filter::NameFilter;
use crate::http::{
    errors::AppError,
    resource::{Resource, ResourceKey},
};
use crate::index::{
    filter::IndexFilter,
    record::IndexRecord,
    results::{IndexResult, IndexResultSet},
};
use async_trait::async_trait;
use bytes::Bytes;
use dotenv;
use futures::TryStreamExt;
use sqlx::{
    postgres::{PgConnectOptions, PgPool, PgPoolOptions},
    Column, ConnectOptions, Row,
};
use std::str::FromStr;
use std::{collections::HashMap, str};

#[derive(Debug, sqlx::FromRow)]
pub struct ResourceRow {
    pub path: String,
    pub name: String,
    pub modified: String,
    pub data: Vec<u8>,
    pub views: Vec<u8>,
    pub version: i32,
    pub viewentry_ids: Vec<String>,
    pub encoding: Option<String>,
}

pub struct PostgresDB {
    pool: PgPool,
}

enum Value {
    String(String),
    Bytes(Vec<u8>),
}

impl PostgresDB {
    async fn create_resources_table(&self) -> Result<(), AppError> {
        sqlx::query(
            "CREATE TABLE IF NOT EXISTS resources (
            \"path\" varchar NOT NULL COLLATE \"C\",
            \"name\" varchar NOT NULL COLLATE \"C\",
            \"modified\" varchar NOT NULL,
            \"data\" bytea NOT NULL,
            \"views\" bytea NULL,
            \"version\" int4 NOT NULL,
            \"viewentry_ids\" varchar [] NULL COLLATE \"C\",
            \"encoding\" varchar NULL COLLATE \"C\"
        )",
        )
        .execute(&self.pool)
        .await?;
        sqlx::query(
            "CREATE UNIQUE INDEX IF NOT EXISTS resources_idx ON resources USING btree (path, name)",
        )
        .execute(&self.pool)
        .await?;
        Ok(())
    }
    async fn create_index_table(&self) -> Result<(), AppError> {
        sqlx::query(
            "CREATE TABLE IF NOT EXISTS resources_index (
            \"path\" varchar NOT NULL COLLATE \"C\",
            \"viewname\" varchar NOT NULL COLLATE \"C\",
            \"fieldname\" varchar NOT NULL COLLATE \"C\",
            \"fieldvalue\" bytea NOT NULL,
            \"resource_name\" varchar NOT NULL COLLATE \"C\",
            \"viewentry_id\" varchar NOT NULL COLLATE \"C\"
        )",
        )
        .execute(&self.pool)
        .await?;
        sqlx::query("CREATE INDEX IF NOT EXISTS resources_index_full_idx ON resources_index USING btree (path, viewname, fieldname, fieldvalue, resource_name, viewentry_id)").execute(&self.pool).await?;
        Ok(())
    }
}

#[async_trait]
impl DB for PostgresDB {
    fn create() -> Box<dyn DB> {
        let url = &dotenv::var("DATABASE_URL").unwrap();
        let mut max_connections = 64;
        if let Ok(dmc) = &dotenv::var("DATABASE_MAX_CONNECTIONS") {
            if let Ok(mc) = dmc.parse() {
                max_connections = mc;
            }
        };
        let mut options = match PgConnectOptions::from_str(url) {
            Ok(o) => o,
            Err(e) => panic!("{}", e),
        };
        options.disable_statement_logging();
        let pool = PgPoolOptions::new()
            .max_connections(max_connections)
            .connect_lazy_with(options);
        Box::new(PostgresDB { pool: pool })
    }

    async fn init(&self) -> Result<(), AppError> {
        self.create_resources_table().await?;
        self.create_index_table().await?;
        Ok(())
    }

    async fn insert<'a>(&self, r: &'a mut Resource) -> Result<bool, AppError> {
        let q = "INSERT INTO resources (path, name, modified, data, views, version, viewentry_ids, encoding) VALUES ($1,$2,$3,$4,$5,$6,$7,$8)";
        let result = sqlx::query(q)
            .bind(&r.path)
            .bind(&r.name)
            .bind(&r.modified)
            .bind(&r.data_as_gzip().unwrap())
            .bind(&r.views_as_gzip().unwrap())
            .bind(&r.version)
            .bind(&r.viewentry_ids[..])
            .bind(&r.encoding)
            .execute(&self.pool)
            .await?;
        Ok(result.rows_affected() != 0)
    }

    async fn update<'a>(&self, r: &'a mut Resource, old_version: i32) -> Result<bool, AppError> {
        let q = "UPDATE resources SET (path, name, modified, data, views, version, viewentry_ids, encoding) = ($1,$2,$3,$4,$5,$6,$7,$8) WHERE path = $9 AND name = $10 AND version = $11";
        let result = sqlx::query(&q)
            .bind(&r.path)
            .bind(&r.name)
            .bind(&r.modified)
            .bind(&r.data_as_gzip().unwrap())
            .bind(&r.views_as_gzip().unwrap())
            .bind(&r.version)
            .bind(&r.viewentry_ids[..])
            .bind(&r.encoding)
            .bind(&r.path)
            .bind(&r.name)
            .bind(old_version)
            .execute(&self.pool)
            .await?;
        Ok(result.rows_affected() != 0)
    }

    async fn get(&self, rkey: &ResourceKey) -> Result<Option<Resource>, AppError> {
        let result = sqlx::query_as::<sqlx::Postgres, ResourceRow>(
            "SELECT path, name, modified, data, views, version, viewentry_ids, encoding FROM resources WHERE path = $1 AND name = $2",
        )
        .bind(&rkey.dir)
        .bind(&rkey.name)
        .fetch_one(&self.pool)
        .await;

        match result {
            Ok(row) => Ok(Some(Resource::new(
                row.path,
                row.name,
                Bytes::from(row.data),
                Some(Bytes::from(row.views)),
                row.version,
                row.modified,
                row.viewentry_ids,
                row.encoding,
                true,
            )?)),
            Err(sqlx::Error::RowNotFound) => Ok(None),
            Err(e) => Err(e.into()),
        }
    }

    async fn delete(&self, rkey: &ResourceKey) -> Result<bool, AppError> {
        let q = "DELETE FROM resources WHERE path = $1 AND name = $2";
        sqlx::query(q)
            .bind(&rkey.dir)
            .bind(&rkey.name)
            .execute(&self.pool)
            .await?;
        Ok(true)
    }
    async fn list_files(
        &self,
        path: &str,
        filter: &NameFilter,
        limit: i32,
    ) -> Result<Vec<FileRow>, AppError> {
        log::debug!(
            "list_files path: {} filter: {:?} limit: {}",
            path,
            filter,
            limit,
        );
        let q = format!(
            "SELECT name FROM resources WHERE path = $1 AND name >= $2 AND name < $3 ORDER BY name {} LIMIT $4",
            &filter.order_as_string()
        );
        let result = sqlx::query_as::<sqlx::Postgres, FileRow>(&q)
            .bind(path)
            .bind(&filter.rangestart)
            .bind(&filter.rangeend)
            .bind(limit)
            .fetch_all(&self.pool)
            .await?;
        Ok(result)
    }

    async fn list_dirs(
        &self,
        path: &str,
        filter: &NameFilter,
        limit: i32,
    ) -> Result<Vec<FileRow>, AppError> {
        log::debug!(
            "list_dirs path: {} filter: {:?} limit: {}",
            path,
            filter,
            limit,
        );
        let q = format!(
            "SELECT DISTINCT SPLIT_PART(path,'/',$1) as name FROM resources WHERE path > $2 AND path >= $3 AND path < $4 ORDER BY name {} LIMIT $5",
            &filter.order_as_string()
        );
        let result = sqlx::query_as::<sqlx::Postgres, FileRow>(&q)
            .bind(path.matches("/").count() as i32 + 1)
            .bind(format!("{}", path))
            .bind(format!("{}{}", path, filter.rangestart))
            .bind(format!("{}{}", path, filter.rangeend))
            .bind(limit)
            .fetch_all(&self.pool)
            .await?;
        Ok(result)
    }
    async fn list_views(
        &self,
        path: &str,
        filter: &NameFilter,
        limit: i32,
    ) -> Result<Vec<FileRow>, AppError> {
        log::debug!(
            "list_views path: {} filter: {:?} limit: {}",
            path,
            filter,
            limit,
        );
        let q = format!(
            "SELECT DISTINCT viewname AS name FROM resources_index WHERE path = $1 AND viewname >= $2 AND viewname < $3 ORDER BY viewname {} LIMIT $4",
            &filter.order_as_string()
        );
        let result = sqlx::query_as::<sqlx::Postgres, FileRow>(&q)
            .bind(path)
            .bind(&filter.rangestart)
            .bind(&filter.rangeend)
            .bind(limit)
            .fetch_all(&self.pool)
            .await?;
        Ok(result)
    }

    async fn insert_index_rows(&self, rows: Vec<&IndexRecord>) -> Result<(), AppError> {
        log::debug!("insert_index_rows: {:?}", rows);
        let mut v1: Vec<&str> = Vec::with_capacity(rows.len());
        let mut v2: Vec<&str> = Vec::with_capacity(rows.len());
        let mut v3: Vec<&str> = Vec::with_capacity(rows.len());
        let mut v4: Vec<&[u8]> = Vec::with_capacity(rows.len());
        let mut v5: Vec<&str> = Vec::with_capacity(rows.len());
        let mut v6: Vec<String> = Vec::with_capacity(rows.len());
        rows.iter().for_each(|row| {
            v1.push(&row.path);
            v2.push(&row.viewname);
            v3.push(&row.fieldname);
            v4.push(&row.fieldvalue);
            v5.push(&row.name);
            v6.push(row.viewentry.read().unwrap().id.clone());
        });
        sqlx::query("INSERT INTO resources_index (path, viewname, fieldname, fieldvalue, resource_name, viewentry_id) SELECT * FROM UNNEST ($1,$2,$3,$4,$5,$6)")
        .bind(v1)
        .bind(v2)
        .bind(v3)
        .bind(v4)
        .bind(v5)
        .bind(v6)
        .execute(&self.pool)
        .await?;
        Ok(())
    }

    async fn delete_index_rows(&self, rows: Vec<&IndexRecord>) -> Result<(), AppError> {
        log::debug!("delete_index_rows: {}", rows.len());
        let q = "DELETE FROM resources_index WHERE path = $1 AND viewname = $2 AND fieldname = $3 AND fieldvalue = $4 AND resource_name = $5 AND viewentry_id = $6";
        for row in rows {
            let veid = row.viewentry.read().unwrap().id.clone();
            sqlx::query(q)
                .bind(&row.path)
                .bind(&row.viewname)
                .bind(&row.fieldname)
                .bind(&row.fieldvalue)
                .bind(&row.name)
                .bind(&veid)
                .execute(&self.pool)
                .await?;
        }
        Ok(())
    }
    async fn filter_index(
        &self,
        path: &str,
        viewname: &str,
        namefilter: &NameFilter,
        indexfilter: &IndexFilter,
        limit: i32,
    ) -> Result<IndexResultSet, AppError> {
        log::debug!("filter_index path: {} limit: {}", path, limit,);
        let mut query = "SELECT idx.*".to_string();
        if !indexfilter.nameonly {
            query.push_str(", rsrc.viewentry_ids, rsrc.views ");
        }
        query.push_str("FROM (SELECT t0.path, t0.resource_name, t0.viewentry_id ");
        for (i, key) in indexfilter.ranges.keys().enumerate() {
            query.push_str(&format!(", t{}.\"{}\"", i, key));
        }
        query.push_str(" FROM ");
        let mut i = 1;
        let mut t = 0;
        let wscursor = Cursor::from_bytes(&indexfilter.cursor);
        let mut values: Vec<Value> = Vec::new();
        if !indexfilter.ranges.is_empty() {
            log::debug!(
                "name range: {} - {}",
                namefilter.rangestart,
                namefilter.rangeend
            );
            log::debug!("indexfilter: {:?}", indexfilter);
            for (key, (rangestart, rangeend)) in indexfilter.ranges.iter() {
                if i > 1 {
                    query.push_str("INNER JOIN ");
                }
                query.push_str(&format!(
                    "(SELECT path, resource_name, viewentry_id, fieldvalue AS \"{}\" FROM resources_index WHERE path = ${} AND viewname = ${} AND resource_name >= ${} AND resource_name < ${} AND fieldname = ${} ",
                    key,
                    i,
                    i+1,
                    i+2,
                    i+3,
                    i+4,
                ));
                i += 5;
                values.push(Value::String(path.to_string()));
                values.push(Value::String(viewname.to_string()));
                values.push(Value::String(namefilter.rangestart.to_string()));
                values.push(Value::String(namefilter.rangeend.to_string()));
                values.push(Value::String(key.to_string()));
                if rangestart.len() > 0 {
                    query.push_str(&format!("AND fieldvalue LIKE ${} ", i));
                    let type_prefix = [indexfilter.get_type_prefix(key), "%".as_bytes()].concat();
                    values.push(Value::Bytes(type_prefix));
                    query.push_str(&format!("AND fieldvalue >= ${} ", i + 1));
                    values.push(Value::Bytes(rangestart.to_vec()));
                    i += 2;
                }
                if rangeend.len() > 0 {
                    query.push_str(&format!("AND fieldvalue LIKE ${} ", i));
                    let type_prefix = [indexfilter.get_type_prefix(key), "%".as_bytes()].concat();
                    values.push(Value::Bytes(type_prefix));
                    query.push_str(&format!("AND fieldvalue < ${} ", i + 1));
                    values.push(Value::Bytes(rangeend.to_vec()));
                    i += 2;
                }
                if let Some(ref cursor) = wscursor {
                    log::debug!("cursor: {:?}", cursor);
                    if !cursor.path.eq(path) {
                        return Err(AppError::new(
                            500,
                            &format!(
                                "Invalid cursor {}",
                                str::from_utf8(&indexfilter.cursor).unwrap()
                            ),
                        ));
                    }
                    let op = if indexfilter.order_by.1.eq("desc") {
                        "<"
                    } else {
                        ">"
                    };
                    query.push_str(&format!("AND (fieldvalue {} ${} ", op, i));
                    values.push(Value::Bytes(cursor.fieldvalue.to_vec()));
                    query.push_str(&format!(
                        "OR (fieldvalue = ${} AND resource_name > ${}) ",
                        i + 1,
                        i + 2
                    ));
                    values.push(Value::Bytes(cursor.fieldvalue.to_vec()));
                    values.push(Value::String(cursor.name.clone()));
                    query.push_str(&format!(
                        "OR (fieldvalue = ${} AND resource_name = ${} AND viewentry_id > ${})) ",
                        i + 3,
                        i + 4,
                        i + 5
                    ));
                    values.push(Value::Bytes(cursor.fieldvalue.to_vec()));
                    values.push(Value::String(cursor.name.clone()));
                    values.push(Value::String(cursor.viewentry_id.clone()));
                    i += 6;
                }
                query.push_str(&format!(") AS t{} ", t));
                if t > 0 {
                    query.push_str(&format!(
                        "ON t{}.viewentry_id = t{}.viewentry_id ",
                        t - 1,
                        t
                    ));
                }
                t += 1;
            }
        } else {
            query.push_str(
                "(SELECT DISTINCT resource_name FROM resources_index WHERE path = $1 AND viewname = $2) AS t0 ",
            );
            values.push(Value::String(path.to_string()));
            values.push(Value::String(viewname.to_string()));
        }
        query.push_str(") AS idx ");
        if !indexfilter.nameonly {
            query.push_str("INNER JOIN (SELECT path, name, viewentry_ids, views FROM resources) AS rsrc ON idx.path = rsrc.path AND idx.resource_name = rsrc.name ");
        }
        query.push_str("ORDER BY ");
        if indexfilter.order_by.0.len() > 0 {
            query.push_str(&format!(
                "\"{}\" {} ,",
                &indexfilter.order_by.0, &indexfilter.order_by.1
            ));
        }
        query.push_str("resource_name asc, viewentry_id asc ");
        query.push_str(&format!("LIMIT ${}", i));
        let mut q = sqlx::query(&query);
        for value in values.iter() {
            match value {
                Value::String(v) => {
                    log::debug!("string value: {}", v);
                    q = q.bind(v)
                }
                Value::Bytes(v) => {
                    log::debug!("string value: {}", str::from_utf8(v).unwrap());
                    log::debug!("byte value: {:?}", v);
                    q = q.bind(v)
                }
            }
        }
        let mut limit2 = limit;
        if let Some(ref cursor) = wscursor {
            limit2 += cursor.row_idx as i32;
        }
        log::debug!("limit: {}", limit);
        log::debug!("limit2: {}", limit2);
        q = q.bind(limit2 + 1); // fetch one more to check if there are more values in db
        log::debug!("before fetch");
        let mut rows = q.fetch(&self.pool);
        log::debug!("after fetch");
        let mut results: Vec<IndexResult> = Vec::new();
        let mut name = "".to_string();
        let mut viewentry_id = "".to_string();
        let mut next_cursor = Vec::new();
        let mut row_idx = 0;
        while let Some(row) = rows.try_next().await? {
            log::debug!("{} ROWS FETCHED", results.len() + 1);
            let mut views = None;
            let mut viewentry_ids = None;
            let mut values: HashMap<String, Vec<Vec<u8>>> = HashMap::new();
            for (i, col) in row.columns().iter().enumerate() {
                log::debug!("column: {}", col.name());
                match col.name() {
                    "path" => continue,
                    "resource_name" => name = row.try_get(i)?,
                    "viewentry_id" => viewentry_id = row.try_get(i)?,
                    "viewentry_ids" => viewentry_ids = row.try_get(i)?,
                    "views" => views = row.try_get(i)?,
                    _ => {
                        let val: Vec<u8> = row.try_get(i)?;
                        values
                            .entry(col.name().to_string())
                            .or_insert(Vec::new())
                            .push(val);
                    }
                }
            }
            let ie = IndexEntry {
                viewname: viewname.to_string(),
                fieldname: indexfilter.ranges.keys().next().unwrap().to_string(),
                fieldvalue: values.values().next().unwrap().first().unwrap(),
                name: name.to_string(),
                viewentry_id: viewentry_id.to_string(),
            };
            let ir = IndexResult {
                name: ie.name,
                viewentry_id: ie.viewentry_id,
                columns: values,
                views: views,
                viewentry_ids: viewentry_ids,
            };
            if let Some(lr) = results.last() {
                if ir.eq(lr) {
                    row_idx += 1;
                    log::debug!("{} equal results found", row_idx + 1);
                } else {
                    row_idx = 0;
                }
            }
            if results.len() == limit as usize {
                let last_result = results.last().unwrap();
                let fieldvalue = last_result
                    .columns
                    .values()
                    .next()
                    .unwrap()
                    .get(0)
                    .unwrap()
                    .clone();
                log::debug!("FIELDVALUE: {:?}", fieldvalue);
                let c = Cursor {
                    path: path.to_string(),
                    name: last_result.name.clone(),
                    viewentry_id: last_result.viewentry_id.clone(),
                    fieldvalue: &fieldvalue,
                    row_idx: row_idx,
                };
                log::debug!("NEW CURSOR: {:?}", c);
                next_cursor = c.to_bytes();
                log::debug!("NEW CURSOR BYTES: {:?}", next_cursor);
                break;
            }
            results.push(ir);
        }
        Ok(IndexResultSet::new(results, &next_cursor))
    }

    async fn schedule_backup(&'static self) {
        log::info!("postgres has no backup implementation yet");
    }
}
