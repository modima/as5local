use std::cmp::Ordering;
use std::collections::HashMap;
use std::sync::{Arc,  RwLock};
use std::time::{Duration, Instant};
use itertools::Itertools;
use log::info;
use pad::PadStr;
use crate::DebugAsyncTiming;

#[derive(Clone)]
pub struct QuantilStatItem {
    created: Instant,
    duration: Duration,
}

#[derive(Clone)]
pub struct QuantilStat {
    last_log: Arc<RwLock<Instant>>,
    last_update: Arc<RwLock<Instant>>,
    data_map: Arc<RwLock<HashMap<String, Vec<QuantilStatItem>>>>,
}

impl Eq for QuantilStatItem {}

impl PartialEq<Self> for QuantilStatItem {
    fn eq(&self, other: &Self) -> bool {
        self.duration.eq(&other.duration)
    }
}

impl PartialOrd<Self> for QuantilStatItem {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.duration.cmp(&other.duration))
    }
}

impl Ord for QuantilStatItem {
    fn cmp(&self, other: &Self) -> Ordering {
        self.duration.cmp(&other.duration)
    }
}
impl QuantilStat {
    pub fn new() -> QuantilStat {
        QuantilStat {
            last_log: Arc::new(RwLock::new(Instant::now())),
            last_update: Arc::new(RwLock::new(Instant::now())),
            data_map: Arc::new(RwLock::new(HashMap::new())),
        }
    }


    pub fn add_value(&self, key: String, duration: Duration) {
        // 1. prüfen, ob die stat zurück gesetzt werden muss
        self.check_reset_and_print();

        match self.last_update.write() {
            Ok(mut val) => {
                *val = Instant::now();
            }
            Err(_) => { return; }
        }

        match self.data_map.write() {
            Ok(mut map) => {
                map
                    .entry(key)
                    .or_insert(vec![])
                    .push( QuantilStatItem{ created: Instant::now(), duration });
            }
            Err(_) => {}
        }
    }
    pub fn update_data(&self, debug_item: &DebugAsyncTiming) {
        // 1. prüfen, ob die stat zurück gesetzt werden muss
        self.check_reset_and_print();

        match self.last_update.write() {
            Ok(mut val) => {
                *val = Instant::now();
            }
            Err(_) => { return; }
        }

        match debug_item.get_entries() {
            Ok(entries) => {
                if entries.len() > 0 {
                    match self.data_map.write() {
                        Ok(mut map) => {
                            for entry in entries {
                                map
                                    .entry(entry.get_description())
                                    .or_insert(vec![])
                                    .push(QuantilStatItem{created: Instant::now(), duration: entry.get_duration().clone() });
                            }
                        }
                        Err(_) => {}
                    }
                }
            }
            Err(_) => {}
        }
    }

    fn check_reset_and_print(&self) {
        let last_update = match self.last_update.read() {
            Ok(val) => {
                val.elapsed()
            }
            Err(_) => {
                return;
            }
        };

        let last_log = match self.last_log.read() {
            Ok(val) => {
                val.elapsed()
            }
            Err(_) => {
                return;
            }
        };


        // ausgeben der stat
        if last_log > last_update && last_log.as_millis() > 30000 {
            match self.last_log.write() {
                Ok(mut last_log) => {
                    *last_log = Instant::now();
                }
                Err(_) => {}
            }

            // bereinigen der Map
            let map_clone = match self.data_map.write() {
                Ok(mut map) => {
                    map.retain(|_, value| {
                        value.retain(|y| y.created.elapsed().as_secs() < 120);
                        value.len() > 0
                    });
                    map.clone()
                }
                Err(_) => {
                    return;
                }
            };


            let quantiles = [0.05, 0.10, 0.5, 0.95, 0.99];

            let mut max_length = 0;
            for key in map_clone.keys() {
                if key.len() > max_length {
                    max_length = key.len();
                }
            }

            max_length += 2;
            let mut stat = format!("\nquantile stat - {}", "".pad_to_width(max_length));

            stat.push_str(format!("\tcount").pad_to_width(12).as_str());
            for quantile in quantiles {
                stat.push_str(format!("\t{}", quantile).pad_to_width(12).as_str());
            }
            stat.push_str("\n");

            for (key, value) in map_clone.iter().sorted() {
                let mut value = value.to_vec();

                value.sort();

                let mut row = key.pad_to_width(max_length).clone();
                row.push_str(format!("\t{:?}", value.len()).pad_to_width(12).as_str());
                for quantile in quantiles {
                    let dur = if let Some(value) = value.get((quantile * value.len() as f64) as usize) {
                        value.duration.clone()
                    } else {
                        Duration::from_millis(0)
                    };

                    row.push_str(format!("\t{:?}", dur).pad_to_width(12).as_str());
                }

                stat.push_str(format!("quantile stat - {}\n", row).as_str());
            }

            info!("{}", stat);
        }

        // long times no update - reset
        if last_update.as_millis() > 60000 {
            info!("quantile stat - RESET");

            // print stat before reset
            match self.data_map.write() {
                Ok(mut map) => {
                    map.clear();
                }
                Err(_) => {}
            }
        }
    }
}