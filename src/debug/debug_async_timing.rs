use std::cmp::Ordering;
use std::fmt::{Debug, Formatter};
use std::sync::{Arc, RwLock};
use std::time::{Duration, Instant};
use crate::ResourceKey;

pub struct DebugAsyncTiming {
    create_time: Instant,
    last_timestamp: Arc<RwLock<Instant>>,
    method: String,
    path: String,
    time_vector: Arc<RwLock<Vec<DebugAsyncTimingItem>>>,
}

#[derive(Clone)]
pub struct DebugAsyncTimingItem {
    index: usize,
    duration: Duration,
    description: String,
}


impl Eq for DebugAsyncTimingItem {}

impl PartialEq<Self> for DebugAsyncTimingItem {
    fn eq(&self, other: &Self) -> bool {
        self.index.eq(&other.index)
    }
}

impl PartialOrd<Self> for DebugAsyncTimingItem {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.index.cmp(&other.index))
    }
}

impl Ord for DebugAsyncTimingItem {
    fn cmp(&self, other: &Self) -> Ordering {
        self.index.cmp(&other.index)
    }
}

impl DebugAsyncTimingItem {
    pub fn get_description(&self) -> String {
        self.description.clone()
    }

    pub fn get_duration(&self)->&Duration {
        &self.duration
    }
}

impl DebugAsyncTiming {
    pub fn new(method: String, rkey: &ResourceKey) -> DebugAsyncTiming {
        DebugAsyncTiming { method, path: rkey.get_path(), create_time: Instant::now(), last_timestamp: Arc::new(RwLock::new(Instant::now())), time_vector: Arc::new(RwLock::new(Vec::new())) }
    }

    pub fn get_entries(&self) -> Result<Vec<DebugAsyncTimingItem>, String> {
        match self.time_vector.read() {
            Ok(data) => {
                Ok(data.clone())
            }
            Err(_) => {
                Err("Poison".to_string())
            }
        }
    }

    pub fn get_total_duration(&self) -> Duration {
        self.create_time.elapsed()
    }

    pub fn measure_time(&self, desc: String) {
        let dur = match self.last_timestamp.write() {
            Ok(mut time_instance) => {
                let ret_val = *time_instance;
                *time_instance = Instant::now();
                ret_val.elapsed()
            }
            Err(_) => {
                return;
            }
        };

        match self.time_vector.write() {
            Ok(mut list) => {
                let idx = list.len() + 1;
                list.push(DebugAsyncTimingItem {
                    index: idx,
                    duration: dur,
                    description: format!("{}-{}: {}", self.method, idx, desc),
                });
                list.sort();
            }
            Err(_) => {}
        }
    }
}

impl Debug for DebugAsyncTiming {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let _ = write!(f, "debug timing - method: {} request: {} - total-duration: {:?}\n", self.method, self.path, self.get_total_duration());
        match self.time_vector.read() {
            Ok(list) => {
                for item in list.iter() {
                    let _ = write!(f, "\t\t\tdebug timing - idx: {} desc: {} duration: {:?}\n", item.index, item.description, item.duration);
                }
            }
            Err(_) => {}
        }
        Ok(())
    }
}