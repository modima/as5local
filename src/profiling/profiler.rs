use crate::{RequestHandler, RequestType};
use std::thread;
use std::time::Duration;
use std::time::{Instant, SystemTime, UNIX_EPOCH};
use std::{
    collections::HashMap,
    sync::{
        {Arc, Mutex},
    },
};
use crossbeam::channel::{Receiver, Sender, unbounded};

use systemstat::{saturating_sub_bytes, Platform, System};
use tokio::time;
use tokio::time::interval;

pub enum MessageType {
    Start,
    Stop,
    Print,
}

pub struct Message {
    mtype: MessageType,
    rtype: Option<RequestType>,
    ip: Option<String>,
    tid: Option<String>,
    time: u128,
}

impl<'a> Message {
    pub fn new(
        mtype: MessageType,
        id: Option<String>,
        rtype: Option<RequestType>,
        ip: Option<String>,
    ) -> Message {
        Message {
            mtype: mtype,
            rtype: rtype,
            ip: ip,
            tid: id,
            time: SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .unwrap()
                .as_millis(),
        }
    }
}

#[derive(Debug)]
pub struct ThreadStats {
    pub rtype: RequestType,
    pub ip: String,
    pub time_start: u128,
}

impl ThreadStats {
    fn new(rtype: RequestType, ip: String, time_start: u128) -> ThreadStats {
        ThreadStats {
            rtype: rtype,
            ip: ip,
            time_start: time_start,
        }
    }

    fn stop(&self, time_stop: u128) -> u32 {
        (time_stop - self.time_start) as u32
    }
}

pub struct Profiler {
    rh: Arc<RequestHandler>,
    tx: Option<Sender<Message>>,
    rx: Option<Receiver<Message>>,
    start_time: Instant,
    reset_time: Instant,
    reset_count: u16,
    request_count: HashMap<RequestType, u32>,
    request_wct: HashMap<RequestType, u32>,
    threads: HashMap<String, ThreadStats>,
    ip_connections: HashMap<String, u16>,
    max_ip_connections: HashMap<String, u16>,
    sys: System,
}

impl<'a> Profiler {
    pub fn new(rh: Arc<RequestHandler>) -> Profiler {
        let p: Profiler = Profiler {
            rh,
            tx: None,
            rx: None,
            start_time: Instant::now(),
            reset_time: Instant::now(),
            reset_count: 0,
            request_count: HashMap::new(),
            request_wct: HashMap::new(),
            threads: HashMap::new(),
            ip_connections: HashMap::new(),
            max_ip_connections: HashMap::new(),
            sys: System::new(),
        };
        p
    }

    pub fn get_sender(&self) -> Option<Sender<Message>> {self.tx.clone()}

    pub fn create_channel(&mut self) -> Sender<Message> {
        let (tx, rx) = unbounded();

        self.tx = Some(tx.clone());
        self.rx = Some(rx);
        tx
    }

    pub fn print_stats(&self) {
        // system stats
        if let Ok(cpu) = self.sys.cpu_load_aggregate() {
            log::info!("Measuring CPU load...");
            thread::sleep(Duration::from_secs(1));
            let cpu = cpu.done().unwrap();
            log::info!(
                "CPU load:\t\t{:.1}% user, {:.1}% nice, {:.1}% system, {:.1}% intr, {:.1}% idle ",
                cpu.user * 100.0,
                cpu.nice * 100.0,
                cpu.system * 100.0,
                cpu.interrupt * 100.0,
                cpu.idle * 100.0
            );
        }

        if let Ok(loadavg) = self.sys.load_average() {
            log::info!(
                "Load average:\t{} {} {}",
                loadavg.one,
                loadavg.five,
                loadavg.fifteen
            );
        }

        if let Ok(mem) = self.sys.memory() {
            log::info!(
                "Memory:\t\t{} used / {}",
                saturating_sub_bytes(mem.total, mem.free),
                mem.total
            );
        }

        if let Ok(stats) = self.sys.socket_stats() {
            log::info!(
                "TCP sockets:\t\t{} in use, {} orphaned",
                stats.tcp_sockets_in_use,
                stats.tcp_sockets_orphaned
            );
        }

        if let Ok(netifs) = self.sys.networks() {
            for netif in netifs.values() {
                if !netif.name.eq("lo") {
                    if let Ok(nif) = self.sys.network_stats(&netif.name) {
                        log::info!(
                            "{} network errors:\t{} rx, {} tx",
                            netif.name,
                            nif.rx_errors,
                            nif.tx_errors,
                        );
                    }
                }
            }
        }

        let duration_millis = Instant::now().duration_since(self.reset_time).as_millis();
        let runtime = Instant::now().duration_since(self.start_time);
        let seconds = runtime.as_secs() % 60;
        let minutes = (runtime.as_secs() / 60) % 60;
        let hours = (runtime.as_secs() / 60) / 60;
        let days = ((runtime.as_secs() / 60) / 60) / 24;
        log::info!(
            "Uptime:\t\t{} days, {:02}:{:02}:{:02} h",
            days,
            hours,
            minutes,
            seconds
        );
        log::info!(
            "Measure interval:\t{}s",
            (duration_millis as f32 / 1000.0).ceil()
        );

        let mut total_requests: u32 = 0;
        let mut total_wct: u32 = 0;
        for (rtype, count) in self.request_count.iter() {
            total_requests += *count;
            let requests_per_second = if duration_millis > 0 {
                *count as f32 / duration_millis as f32 * 1000.0
            } else {
                0.0
            };
            let mut wct_mean: f64 = 0.0;
            if let Some(wct) = self.request_wct.get(&rtype) {
                total_wct += wct;
                wct_mean = (*wct as f64 / *count as f64) / 1000.0
            }
            log::info!("{} requests/s:\t{:.1}", rtype.as_ref(), requests_per_second);
            log::info!("{} mean wct:\t{:.3}s", rtype.as_ref(), wct_mean);
        }
        let requests_per_second = if duration_millis > 0 {
            total_requests as f32 / duration_millis as f32 * 1000.0
        } else {
            0.0
        };
        let wct_mean = if total_requests > 0 {
            (total_wct as f64 / total_requests as f64) / 1000.0
        } else {
            0.0
        };
        log::info!("Total requests:\t{}", total_requests);
        log::info!("Total requests/s:\t{:.1}", requests_per_second);
        log::info!("Total mean wct:\t{:.3}s", wct_mean);
        log::info!(
            "Tab separated:\t{}\t{:.1}\t{:.3}",
            total_requests,
            requests_per_second,
            wct_mean
        );
        if !self.max_ip_connections.is_empty() {
            log::info!("Parallel requests by IP:");
            for (ip, count) in self.max_ip_connections.iter() {
                log::info!("{}:\t{}", ip, count);
            }
        }
    }
    pub fn reset(&mut self) {
        self.request_count.clear();
        self.request_wct.clear();
        self.max_ip_connections.clear();
        if self.reset_count % 100 == 0 {
            self.threads.clear();
            self.ip_connections.clear();
        }
        self.reset_time = Instant::now();
        self.reset_count += 1;
    }
}

pub async fn start_profiling(tx: Sender<Message>, interval_sec: u64) {
    let mut interval = interval(time::Duration::from_secs(interval_sec));
    loop {
        interval.tick().await;
        match tx.send(Message::new(MessageType::Print, None, None, None)) {
            Ok(_) => {}
            Err(e) => {
                log::warn!("channel error on print stats: {}", e);
            }
        };
    }
}

pub async fn start_receiver(profiler: Arc<Mutex<Profiler>>) {
    let receiver = {
        let mut p = profiler.lock().unwrap();
        p.reset();
        p.rx.clone()
    };
    loop {
        if let Some(r) = &receiver {
            let msg = r.recv().unwrap();
            let mut p = profiler.lock().unwrap();
            match msg.mtype {
                MessageType::Start => {
                    let ip = msg.ip.unwrap();
                    let rtype = msg.rtype.unwrap();
                    log::debug!("start {} from {}", rtype.as_ref(), ip);
                    p.threads.insert(
                        msg.tid.unwrap(),
                        ThreadStats::new(rtype, ip.clone(), msg.time),
                    );
                    *p.request_count.entry(rtype).or_insert(0) += 1;
                    let count = p.ip_connections.entry(ip.clone()).or_insert(0);
                    *count += 1;
                    let val = *count;
                    match p.max_ip_connections.get_mut(&ip) {
                        Some(v) => {
                            if *v < val {
                                *v = val;
                            }
                        }
                        None => {
                            p.max_ip_connections.insert(ip, 1);
                        }
                    }
                }
                MessageType::Stop => {
                    if let Some(t_stats) = p.threads.remove(&msg.tid.unwrap()) {
                        let rtype = t_stats.rtype;
                        let ip = &t_stats.ip;
                        let duration = t_stats.stop(msg.time);
                        log::debug!("stop {} from {} after {}ms", rtype.as_ref(), ip, duration);
                        *p.request_wct.entry(rtype).or_insert(0) += duration;
                        if let Some(count) = p.ip_connections.get_mut(ip) {
                            *count -= 1;
                            if *count == 0 {
                                p.ip_connections.remove(ip);
                            }
                        }
                    }
                }
                MessageType::Print => {
                    let mut request_count = Arc::strong_count(&p.rh);
                    if request_count <= 3 {
                        request_count = 0
                    };
                    log::info!("---------------------------------------");
                    p.print_stats();
                    p.reset();
                    log::info!("Current requests:\t{}", request_count);
                    log::info!("---------------------------------------");
                }
            }
        }
    }
}
