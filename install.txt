1. Install dependencies
-----------------------------------
sudo apt install curl git build-essential gcc make clang llvm librocksdb-dev -y

2. Install and update rust
-----------------------------------
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
rustup update

3. Download source code
-----------------------------------
git clone https://bitbucket.org/modima/as5local

4. Install dependencies
-----------------------------------
cd as5local && cargo update

5. Compile as5local (It is important to build in release mode!)
-----------------------------------
cargo build --release